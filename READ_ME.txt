This project consists of three parts front end, back end and the database. 

In order to run the prototype locally, you first need to install MySQL (version 5.5.42) and run it on port 8889.
Next, you need to create the database with the following options:
    database name - 'bike_web_v2'
    database user - root
    user password - root
After the database is created, you can import the database_dump_v7.sql. Optionally, you can reconfigure file api/internal/config/Database.php to match your environment.

Next, in order to use the API, you need to make sure to have installed PHP server, for example, Apache and have all the files located in /api folder of that server directory.

Lastly, in order to run the Angular part of the prototype, you have to make sure the API path in Angular environment.ts script is correct based on the location of the project. Run 'npm install' to download all the dependencies.
Once the dependencies are downloaded, the application can be run by executing 'ng serve —-ssl’ in the terminal. The application is then accessible via browser at https://localhost:4200.

In order to access the application as a regular user or bike shop administrator, feel free to create new account. There is only one predefined main system admin with the following login information:
e-mail: a@a.com
password: Aa111111111
