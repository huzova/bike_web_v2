<?php
    include '../internal/session.php';
    include '../internal/service/UserService.php';

    try{
        $userMail = $_POST['mail'];
        $userPassword = $_POST['password'];
        
        $userService = UserService::getInstance();
        
        $response = $userService->login($userMail, $userPassword);
        
        if(is_array($response)) {
    
            $timeout = $response['timeout'];
    
            if($timeout) {
                echo '{"status": "'.$response['error'].'", "ok":false, "timeOut":true}';
            } else {
                echo '{"status": "'.$response['error'].'", "ok":false, "timeOut":false}';
            }
        } else {
            unset($response -> userPassword);
            unset($response -> userAgent);
            unset($response -> loginAttemptCount);
            unset($response -> lastLoginAttemptTime);
            echo '{"status": "loggedin", "ok":true, "timeOut":false, "data":'.json_encode($response).'}';
        }   
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "timeOut":true}';
    }
?>