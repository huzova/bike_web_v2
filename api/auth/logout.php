<?php
    include '../internal/session.php';

    // https://stackoverflow.com/a/2241793/2523007
    setcookie (session_id(), "", time() - 3600);
    session_destroy();
    session_write_close();

    echo '{"status": "Logged out", "ok":true}';