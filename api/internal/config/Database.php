<?php

class Database {
    private $conn = null;

    public static function getInstance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new Database();
        }
        return $inst;
    }

    private function __construct() {

    }

    function getMySQLConnection() {
        if($this -> conn == null) {
            $servername = "localhost:8889";
            $username = "root";
            $password = "root";
            $dbname = "bike_web_v2";

            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4") );
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return $conn;
}
    
    function getFileUploadBase() {
        return 'img/upload/';
    }
}
