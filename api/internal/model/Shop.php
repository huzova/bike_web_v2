<?php

class Shop {
    public $shopId;
    public $shopName;
    public $shopEmail;
    public $shopPhone;
    public $shopAddress;
    public $shopWebsite;
    public $pricePoint;
    public $isApproved;
    
    public $latitude;
    public $longitude;

    public $images; // array of image urls
    public $openingHours; // array of opening hours
}