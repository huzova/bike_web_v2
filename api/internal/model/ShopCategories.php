<?php

class ShopCategories {
    public $shopId;
    public $sellsNewBikes;
    public $sells2ndHandBikes;
    public $isRental;
    public $isBikeRepair;
    public $sellsAccessories;
}