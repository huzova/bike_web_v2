<?php

class User {
    public $userId;
    public $userMail;
    public $userName;
    public $userPassword;
    public $img;
    public $imgTmp; // used for saving tmp files

    public $isBikeAdmin;
    public $isAdmin;

    public $userAgent;
    public $lastLoginAttemptTime;
    public $loginAttemptCount;

}