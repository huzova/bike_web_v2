<?php

class FileRepository {

    public static function getInstance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new FileRepository();
        }
        return $inst;
    }

    private function __construct() {

    }

    function uploadAny($from, $to, $fileName) {
        $filePath = $to.'/'.$fileName;
        move_uploaded_file( $from, $filePath);
        return $filePath;
    }

    function uploadUserProfile($user) {
        $from = $user -> imgTmp;
        // always called from /user or /auth or / shop, so needs to be ../
        $to = "../upload";
        $fileName =  $user->userName .'_'. $user -> img;
        $newFile = $this->uploadAny($from, $to, $fileName);

        $user -> img = $newFile;
        // set this new file path to user profile
        unset($user -> imgTmp);
    }

    function uploadShopImage($imgName, $imgTmpPath) {
        $to = "../upload/shop";
        $newFile = $this->uploadAny($imgTmpPath, $to, $imgName);

        echo "$imgName, $imgTmpPath, $to </br>";

        return $newFile;
    }

    function delete($filePath) {
        unlink($filePath);
    }

}
