<?php

require_once(dirname(__FILE__) . '/../config/Database.php');

/****************************************/

class MessageRepository {
    private $database;

    public static function getInstance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new MessageRepository();
        }
        return $inst;
    }

    private function __construct() {
        $this -> database = Database::getInstance();
    }

    /****************************************/
    
    // create msg
    public function create($userId, $message) {
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("INSERT INTO messages 
            (users_user_id, message) 
            VALUES ( :user_id , :message)");
        $query->bindParam(':user_id' , $userId);
        $query->bindParam(':message' , $message);
        $execute = $query->execute();
        return null;
    }

    /****************************************/
    
    // get message by user id
    public function getMsgByUserId($userId){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT messages.message, users.user_name, users.user_mail 
            FROM messages
            INNER JOIN users ON messages.users_user_id = users.user_id
            AND messages.users_user_id = :id");
        $query->bindParam(':id' , $userId);
        $execute = $query->execute();
        $result = $query -> fetchAll();

        $messages = [];

        foreach ($result as $row) {
            array_push($messages, $this -> getMsgFromResult($row));
        }

        return $messages;
    }

    /****************************************/
    
    // get all messages
    public function getMsgs() {
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT messages.message, users.user_name, users.user_mail 
            FROM messages
            INNER JOIN users ON messages.users_user_id = users.user_id");
        $execute = $query->execute();
        $result = $query -> fetchAll();

        $messages = [];

        foreach ($result as $row) {
            array_push($messages, $this -> getMsgFromResult($row));
        }

        return $messages;
    }

    /****************************************/

    // get message from result 
    function getMsgFromResult($result){
        $message = new Message();
        $message -> userMail = $result['user_mail'];
        $message -> userName = $result['user_name'];
        $message -> message = htmlspecialchars($result['message']);
        return $message;
    }

    /****************************************/

}