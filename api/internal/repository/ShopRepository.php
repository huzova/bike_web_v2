<?php

require_once(dirname(__FILE__) . '/../config/Database.php');

/****************************************/

class ShopRepository {
    private $database;

    public static function getInstance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new ShopRepository();
        }
        return $inst;
    }

    private function __construct() {
        $this -> database = Database::getInstance();
    }

    /****************************************/
    /*            SAVE                      */
    /****************************************/

    // save basics
    public function create($shop) {
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("INSERT INTO bikeshops 
            (shop_name, shop_adress, shop_phone, price_point, shop_website, longitude, latitude) 
            VALUES (:shop_name, :shop_address, :shop_phone, :price_point, :website, :longitude, :latitude)");
        $query->bindParam(':shop_name' , $shop-> shopName);
        $query->bindParam(':shop_address' , $shop-> shopAddress);
        $query->bindParam(':shop_phone' , $shop-> shopPhone);
        $query->bindParam(':website' , $shop-> shopWebsite);
        $query->bindParam(':price_point' , $shop-> pricePoint);
        $query->bindParam(':longitude' , $shop-> longitude);
        $query->bindParam(':latitude' , $shop-> latitude);
        $execute = $query->execute();

        $shopId = $conn->lastInsertId();

        return $shopId;
    }

    /****************************************/

    // saveShopCategory
    public function saveShopCategory($data){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("INSERT INTO bikeshop_category 
            (bikeshops_shop_id, sells_new_bikes, sells_2nd_hand_bikes, 
            is_rental, is_bike_repair, sells_accessories) 
            VALUES 
            (:shop_id, :sells_new_bikes, :sells_2nd_hand_bikes,
            :is_rental, :is_bike_repair, :sells_accessories)");
        $query->bindParam(':shop_id' , $data-> shopId);
        $query->bindParam(':sells_new_bikes' , $data-> sellsNewBikes);
        $query->bindParam(':sells_2nd_hand_bikes' , $data-> sells2ndHandBikes);
        $query->bindParam(':is_rental' , $data-> isRental);
        $query->bindParam(':is_bike_repair' , $data-> isBikeRepair);
        $query->bindParam(':sells_accessories' , $data-> sellsAccessories);
        $execute = $query->execute();
    }

    /****************************************/
    
    //save Shop Images
    public function saveShopImages($shop){
        $conn = $this -> database -> getMySQLConnection();

        if(is_array($shop->images))
            foreach ($shop->images as $img) {
                $query = $conn->prepare("INSERT INTO bikeshop_images 
                    (bikeshops_shop_id, img) 
                    VALUES 
                    (:shop_id, :img)");
                $query->bindParam(':shop_id' , $shop-> shopId);
                $query->bindParam(':img' , $img);
                $execute = $query->execute();
            }
    }

  /****************************************/
  
  // save Shop Reviews
  public function saveShopReviews($reviews){
        $conn = $this -> database -> getMySQLConnection();

        if(is_array($reviews))
            foreach ($reviews as $review) {
                // skip if review is too long
                if(strlen($review -> review) > 1000)
                    continue;
                $query = $conn->prepare("INSERT INTO bikeshop_rating_review 
                    (bikeshops_shop_id, rating, user_id, review) 
                    VALUES 
                    (:shop_id, :rating, :user_id, :review)");
                $query->bindParam(':shop_id' , $review-> shopId);
                $query->bindParam(':rating' , $review -> rating);
                $query->bindParam(':user_id' , $review-> userId);
                $query->bindParam(':review' , $review -> review);
                $execute = $query->execute();
            }
    }

    /****************************************/

    // save Shop Review
    public function saveShopReview($review){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("INSERT INTO bikeshop_rating_review 
            (bikeshops_shop_id, rating, user_id, review) 
            VALUES 
            (:shop_id, :rating, :user_id, :review)");
        $query->bindParam(':shop_id' , $review-> shopId);
        $query->bindParam(':rating' , $review -> rating);
        $query->bindParam(':user_id' , $review-> userId);
        $query->bindParam(':review' , $review -> review);
        $execute = $query->execute();
    }

    /****************************************/
    
    // save Shop Opening Hours
    public function saveShopOpeningHours($shop){
        $conn = $this -> database -> getMySQLConnection();

        if(is_array($shop->openingHours))
            foreach ($shop->openingHours as $openingHour) {
                $query = $conn->prepare("INSERT INTO bikeshop_opening_hours 
                    (bikeshops_shop_id, day, open_time, close_time) 
                    VALUES 
                    (:shop_id, :day, :open_time, :close_time)");
                $query->bindParam(':shop_id' , $shop-> shopId);
                $query->bindParam(':day' , $openingHour->day);
                $query->bindParam(':open_time' , $openingHour->openTime);
                $query->bindParam(':close_time' , $openingHour->closeTime);
                $execute = $query->execute();
            }
    }

    /****************************************/
    /*            UPDATE                    */
    /****************************************/

    // update basic
    public function updateShopBasic($data){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("UPDATE bikeshops SET 
            shop_name = :shop_name, shop_adress = :shop_adress, 
            shop_phone = :shop_phone, price_point = :price_point 
            WHERE shop_id = :shop_id");
        $query->bindParam(':shop_id' , $data-> shopId);
        $query->bindParam(':shop_name' , $data-> shopName);
        $query->bindParam(':shop_adress' , $data-> shopAdress);
        $query->bindParam(':shop_phone' , $data-> shopPhone);
        $query->bindParam(':price_point' , $data-> pricePoint);
        $execute = $query->execute();
    }

    /****************************************/

    // update shop - business focus
    public function updateShopCategory($data){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("UPDATE bikeshop_category SET
            sells_new_bikes = :sells_new_bikes, 
            sells_2nd_hand_bikes = :sells_2nd_hand_bikes, 
            is_rental = :is_rental,
            is_bike_repair = :is_bike_repair,
            sells_accessories = :sells_accessories
            WHERE bikeshops_shop_id = :shop_id");
         $query->bindParam(':shop_id' , $data-> shopId);
         $query->bindParam(':sells_new_bikes' , $data-> sellsNewBikes);
         $query->bindParam(':sells_2nd_hand_bikes' , $data-> sells2ndHandBikes);
         $query->bindParam(':is_rental' , $data-> isRental);
         $query->bindParam(':is_bike_repair' , $data-> isBikeRepair);
         $query->bindParam(':sells_accessories' , $data-> sellsAccessories);
         $execute = $query->execute();
    }

    /****************************************/
    /*            CLAIM                     */
    /****************************************/

    // claim shop
    function claimShop($shop_id, $user_id){
        try {
            $conn = $this -> database -> getMySQLConnection();
            $query = $conn->prepare("INSERT INTO bikeshop_admins 
                (bikeshops_shop_id, users_user_id) 
                VALUES (:shop_id,:user_id)");
            $query->bindParam(':shop_id' , $shop_id);
            $query->bindParam(':user_id' , $user_id);
            $execute = $query->execute();
            return null;
        } catch (Exception $e) {
            return "Shop cannot be claimed";
        }
    }

    /****************************************/

    // unclaim shop
    function unclaimShop($shop_id, $user_id){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("DELETE FROM bikeshop_admins 
            WHERE bikeshops_shop_id= :shop_id 
            AND users_user_id = :user_id");
        $query->bindParam(':shop_id' , $shop_id);
        $query->bindParam(':user_id' , $user_id);
        $execute = $query->execute();
    }

    /****************************************/
    /*              GET                     */
    /****************************************/
    function hasReviewInstance($shop_id, $user_id){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT * FROM bikeshop_rating_review 
            WHERE user_id = :user_id 
            AND bikeshops_shop_id = :shop_id;");
        $query->bindParam(':shop_id' , $shop_id);
        $query->bindParam(':user_id' , $user_id);
        $execute = $query->execute();
        $result = $query -> fetchAll();

        if(count($result) > 0){
            return true ;
        } else {
            return false;
        }
    }
    
    /****************************************/
    
    // find shops nearby
    public function findShopsNearby($lon, $lat, $radius) {
        $conn = $this -> database -> getMySQLConnection();
        $sql = "SELECT
            bikeshops.shop_id,
            bikeshops.shop_name,
            bikeshops.shop_adress,
            bikeshops.shop_phone,
            bikeshops.price_point,
            bikeshops.shop_website,
            bikeshops.latitude,
            bikeshops.longitude,
            bikeshop_category.sells_new_bikes,
            bikeshop_category.sells_2nd_hand_bikes,
            bikeshop_category.is_rental,
            bikeshop_category.is_bike_repair,
            bikeshop_category.sells_accessories
            FROM bike_web_v2.bikeshops
            LEFT JOIN bikeshop_category
            ON bikeshops.shop_id = bikeshop_category.bikeshops_shop_id
            WHERE bikeshops.shop_id IN (SELECT shop_id FROM 
            (SELECT shop_id, (6371 * acos(cos(radians(:lat)) *
            cos(radians(latitude)) * cos(radians(longitude)
            - radians(:lng)) + sin(radians(:lat)) * sin(radians(latitude)))) 
            AS distance
            FROM bikeshops
            HAVING distance < :radius
            ORDER BY distance) AS ids) AND is_approved";
        $query = $conn->prepare($sql);
        $query->bindParam(':lng' , $lon);
        $query->bindParam(':lat' , $lat);
        $query->bindParam(':radius' , $radius);
        $execute = $query->execute();
        $shops = [];
        $result = $query -> fetchAll();
        foreach ($result as $row) {
            array_push($shops, $this -> getFullShopFromResult($row));
        }

        return $shops;
    }

    /****************************************/

    // get shop basic + categories by id
    function getShopBasicCategoriesById($shop_id){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT 
            bikeshops.shop_id,  
            bikeshops.shop_name,
            bikeshops.shop_adress, 
            bikeshops.shop_phone, 
            bikeshops.price_point,
            bikeshops.shop_website,
            bikeshops.latitude,
            bikeshops.longitude,
            bikeshop_category.sells_new_bikes,
            bikeshop_category.sells_2nd_hand_bikes,
            bikeshop_category.is_rental,
            bikeshop_category.is_bike_repair,
            bikeshop_category.sells_accessories
            FROM bike_web_v2.bikeshops
            LEFT JOIN bikeshop_category
            ON bikeshops.shop_id = bikeshop_category.bikeshops_shop_id
            WHERE shop_id = :shop_id");
        $query->bindParam(':shop_id' , $shop_id);
        $execute = $query -> execute();
        $result = $query -> fetchAll();
        // var_dump($result);
        if(count($result) == 1){
            $shopObj = $this -> getFullShopFromResult($result[0]);
            $shopObj -> images = $this -> getShopImg($shop_id);
            return $shopObj;
        } else 
            return $result;
    }
    
    function getShopImg($shop_id){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT img 
            FROM bikeshop_images 
            WHERE bikeshops_shop_id = :shop_id");
        $query->bindParam(':shop_id' , $shop_id);
        $execute = $query->execute();
        $result = $query -> fetchAll();
        // var_dump($result);
        return $result;
    }
    /****************************************/

    // get opening hours by shop id 
    function getOpenningHoursByShopId($shop_id){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT day, open_time, close_time, bikeshops_shop_id
            FROM bikeshop_opening_hours 
            WHERE bikeshops_shop_id  = :shop_id");
        $query->bindParam(':shop_id' , $shop_id);
        $execute = $query->execute();
        $result = $query -> fetchAll();

        $openingHours = [];

        if(count($result) != 0) {
            foreach ($result as $row) {
                array_push($openingHours, $this -> getOpeningHoursFromResult($row));
            }
            return $openingHours;
        } else 
            return null;

    
    }

    /****************************************/
    
    //get avg shop rating
    function getAvgShopRating($shop_id){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT AVG(rating) AS avg
            FROM bikeshop_rating_review 
            WHERE bikeshops_shop_id = :shop_id");
        $query->bindParam(':shop_id' , $shop_id);
        $execute = $query->execute();
        $result = $query -> fetchAll();
        return $result[0]['avg'];
    }
    
    /****************************************/

    // get all shops basic + categories
    function getAllShopsBasicCategories($is_approved){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT 
            bikeshops.shop_id,  
            bikeshops.shop_name,
            bikeshops.shop_adress, 
            bikeshops.shop_phone, 
            bikeshops.price_point,
            bikeshops.shop_website,
            bikeshops.latitude,
            bikeshops.longitude,
            bikeshop_category.sells_new_bikes,
            bikeshop_category.sells_2nd_hand_bikes,
            bikeshop_category.is_rental,
            bikeshop_category.is_bike_repair,
            bikeshop_category.sells_accessories
            FROM bike_web_v2.bikeshops
            LEFT JOIN bikeshop_category
            ON bikeshops.shop_id = bikeshop_category.bikeshops_shop_id
            WHERE bikeshops.is_approved = :is_approved");
        $query->bindParam(':is_approved' , $is_approved);
        $execute = $query->execute();
        $result = $query -> fetchAll();
        
        $shops = [];

        foreach ($result as $row) {
            array_push($shops, $this -> getFullShopFromResult($row));
        }

        return $shops;
    }

    /****************************************/

    // get all shops basic + categories to approve 
    function getAllShopsBasicCategoriesToApprove(){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT 
            bikeshops.shop_id,  
            bikeshops.shop_name,
            bikeshops.shop_adress, 
            bikeshops.shop_phone, 
            bikeshops.price_point,
            bikeshops.shop_website,
            bikeshops.latitude,
            bikeshops.longitude,
            bikeshop_category.sells_new_bikes,
            bikeshop_category.sells_2nd_hand_bikes,
            bikeshop_category.is_rental,
            bikeshop_category.is_bike_repair,
            bikeshop_category.sells_accessories
            FROM bike_web_v2.bikeshops
            LEFT JOIN bikeshop_category
            ON bikeshops.shop_id = bikeshop_category.bikeshops_shop_id
            WHERE bikeshops.is_approved = 0");
        $execute = $query->execute();
        $result = $query -> fetchAll();
        
        $shops = [];

        foreach ($result as $row) {
            array_push($shops, $this -> getFullShopFromResult($row));
        }

        return $shops;
    }
    
    /****************************************/

    // get admins by shop id
    function getAdminsByShopId($shop_id){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT users_user_id, users.user_name, users.user_mail
            FROM bikeshop_admins
            INNER JOIN users 
            ON bikeshop_admins.users_user_id = users.user_id
            AND bikeshops_shop_id = :shop_id");
        $query->bindParam(':shop_id' , $shop_id);

        $execute = $query->execute();
        $result = $query -> fetchAll();
        $users = [];

        foreach ($result as $row) {
            array_push($users, $this -> getUserFromResult($row));
        }

        return $users;
    }

    /****************************************/

    // get admins by user id
    function getShopsByUserId($user_id){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT bikeshop_admins.bikeshops_shop_id, bikeshops.shop_name, bikeshops.shop_adress
        FROM bikeshop_admins
        INNER JOIN bikeshops 
        ON bikeshop_admins.bikeshops_shop_id = bikeshops.shop_id
        AND bikeshop_admins.users_user_id = :user_id");
        $query->bindParam(':user_id' , $user_id);

        $execute = $query->execute();
        $result = $query -> fetchAll();
        $shops = [];

        if (count($result) > 0){
            foreach ($result as $row) {
                array_push($shops, $this -> getShortShopFromResult($row));
            }
            return $shops;
        }
        return null;
        
    }

    /****************************************/
    
    // get reviews and rating
    function getReviewsByShopId($shop_id){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT review, rating, user_id 
        FROM bikeshop_rating_review 
        WHERE bikeshops_shop_id = :shop_id");
        $query->bindParam(':shop_id' , $shop_id);
        $execute = $query->execute();
        $result = $query -> fetchAll();
        $reviews = [];

        foreach ($result as $row) {
            array_push($reviews, $this -> getReviewFromResult($row));
        }

        return $reviews;
    }

    /****************************************/
    /*               DELETE                 */
    /****************************************/

    //delete shop
    function deleteByShopId($shop_id){
        $conn = $this -> database -> getMySQLConnection();
        
        try {
            $conn ->beginTransaction();

            $query1 = $conn->prepare("DELETE FROM bikeshop_opening_hours WHERE bikeshops_shop_id= :shop_id");
            $query1->bindParam(':shop_id' , $shop_id);
            $execute = $query1->execute();

            $query2 = $conn->prepare("DELETE FROM bikeshop_category WHERE bikeshops_shop_id= :shop_id");
            $query2->bindParam(':shop_id' , $shop_id);
            $execute = $query2->execute();

            $query3 = $conn->prepare("DELETE FROM bikeshop_admins WHERE bikeshops_shop_id= :shop_id");
            $query3->bindParam(':shop_id' , $shop_id);
            $execute = $query3->execute();

            $query4 = $conn->prepare("DELETE FROM bikeshop_images WHERE bikeshops_shop_id= :shop_id");
            $query4->bindParam(':shop_id' , $shop_id);
            $execute = $query4->execute();

            $query5 = $conn->prepare("DELETE FROM bikeshop_rating_review WHERE bikeshops_shop_id= :shop_id");
            $query5->bindParam(':shop_id' , $shop_id);
            $execute = $query5->execute();

            $query6 = $conn->prepare("DELETE FROM bikeshops WHERE shop_id= :shop_id");
            $query6->bindParam(':shop_id' , $shop_id);
            $execute = $query6->execute();

            $conn->commit();
            return null;
        } catch(Exception $e) {
            $conn->rollback();
            return 'Cannot execute delete shop';
        }
    }

    /****************************************/
    /*               UPDATE                 */
    /****************************************/
    
    // approve Shop
    function approveShop($shop_id){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("UPDATE bikeshops 
            SET is_approved = 1 
            WHERE shop_id= :shop_id");
        $query->bindParam(':shop_id' , $shop_id);
        $execute = $query->execute();
        return null;
    }

    /****************************************/
    /*           CONSTRUCT RESULT           */
    /****************************************/

    // get User From Result
    private function getUserFromResult($result) {
        $user = new User();
        $user -> userId = $result['users_user_id'];
        $user -> userMail = $result['user_mail'];
        $user -> userName = $result['user_name'];
        return $user;
    }

    /****************************************/

    // get short Shop From Result
    private function getShortShopFromResult($result) {
        $shop = new Shop();
        $shop -> shopId = $result['bikeshops_shop_id'];
        $shop -> shopName = $result['shop_name'];
        $shop -> shopAddress = $result['shop_adress'];
        return $shop;
    }

    /****************************************/
    
    // get full Shop From Result
    private function getFullShopFromResult($result) {
        $shop = new Shop();
        $shop -> shopId = base64_encode($result['shop_id']);
        $shop -> shopName = htmlspecialchars($result['shop_name']);
        $shop -> shopAddress = htmlspecialchars($result['shop_adress']); 
        $shop -> shopPhone = isset($result['shop_phone']) ? $result['shop_phone'] : null; 
        $shop -> isApproved = isset($result['is_approved']) ? $result['is_approved'] : null; 
        $shop -> pricePoint = $result['price_point']; 
        $shop -> latitude = $result['latitude'] ? $result['latitude'] : null; // isset($result['latitude']) ? $result['latitude '] : null; 
        $shop -> longitude = $result['longitude']; // isset($result['longitude']) ? $result['longitude '] : null;
        $shop -> shopWebsite = isset($result['shop_website']) ? $result['shop_website'] : null; 
        $shop -> sellsNewBikes = isset($result['sells_new_bikes']) ? $result['sells_new_bikes'] : null; 
        $shop -> sells2ndHandBikes = isset($result['sells_2nd_hand_bikes']) ? $result['sells_2nd_hand_bikes'] : null; 
        $shop -> isRental = isset($result['is_rental']) ? $result['is_rental'] : null; 
        $shop -> isBikeRepair = isset($result['is_bike_repair']) ? $result['is_bike_repair'] : null; 
        $shop -> sellsAccessories =isset( $result['sells_accessories']) ? $result['sells_accessories'] : null; 
        return $shop;
    }
    
    /****************************************/
    
    // get Opening Hours From Result
    private function getOpeningHoursFromResult($result) {
        $openingHours = new OpeningHours ();
        $openingHours -> shopId = $result['bikeshops_shop_id'];
        $openingHours -> day = htmlspecialchars($result['day']);
        $openingHours -> openTime = $result['open_time'];
        $openingHours -> closeTime = $result['close_time'];
        return $openingHours;
    }

    /****************************************/

    // get review from result 
    private function getReviewFromResult($result) {
        $review = new Review();
        $review -> review = htmlspecialchars($result['review']);
        $review -> rating = $result['rating'];
        $review -> userId = $result['user_id']; 
        return $review;
    }

    /****************************************/
    
    // get Not Approved Shops
    function getNotApprovedShops(){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare("SELECT count(is_approved) as total FROM bikeshops WHERE is_approved = 0;");
        $execute = $query->execute();
        $result = $query -> fetchAll();
        return $result[0]['total'];
    }
}

