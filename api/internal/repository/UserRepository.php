<?php

require_once(dirname(__FILE__) . '/../config/Database.php');

/****************************************/

class UserRepository {
    private $database;

    public static function getInstance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new UserRepository();
        }
        return $inst;
    }

    private function __construct() {
        $this -> database = Database::getInstance();
    }
    
    /****************************************/
    /*            SAVE                      */
    /****************************************/

    // create user
    function createUser($user){
        $isAdmin = 0;
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn ->prepare ("INSERT INTO users 
          (user_mail, user_password, user_name, last_login_attempt_time, last_user_agent, is_bikeshop_admin, user_img, is_admin) 
          VALUES 
          (:mail, :password, :name, CURRENT_TIMESTAMP, :last_user_agent, :is_bikeshop_admin, :img, :is_admin)");
        $query->bindParam(':mail' , $user-> userMail);
        $query->bindParam(':password' , $user->userPassword);
        $query->bindParam(':name' , $user->userName);
        $query->bindParam(':img' , $user->img);
        $query->bindParam(':last_user_agent' , $user->userAgent);
        $isBikeShopAdmin = $user->isBikeAdmin ? 1 : 0;
        $query->bindParam(':is_bikeshop_admin' , $isBikeShopAdmin);
        $query->bindParam(':is_admin' , $isAdmin);
        $result = $query->execute();
        $id = $conn->lastInsertId();
        $user -> userId = $id;
    }
    
    /****************************************/
    /*              GET                     */
    /****************************************/

    // get user by id
    function getUserById($userId){
        $conn = $this -> database -> getMySQLConnection();

        $query = $conn->prepare("SELECT * FROM users WHERE user_id = :id");
        $query->bindParam(':id' , $userId);
        $execute = $query->execute();
        $result = $query -> fetchAll(\PDO::FETCH_ASSOC);

        if(count($result) == 1){
            return $this -> getUserFromResult($result[0]);
        }
        else
            return null;
    }
    /****************************************/

    // get user password
    function getUserPass($userId){
        $conn = $this -> database -> getMySQLConnection();

        $query = $conn->prepare("SELECT user_password FROM users WHERE user_id = :id");
        $query->bindParam(':id' , $userId);
        $execute = $query->execute();
        $result = $query -> fetchAll(\PDO::FETCH_ASSOC);

        if(count($result) == 1){
            return $this -> getUserPassFromResult($result[0]);
        }
        else{
            return null;
        }
    }

    /****************************************/

    // is user bike admin
    function isBikeAdmin($userId){
        $conn = $this -> database -> getMySQLConnection();

        $query = $conn->prepare("SELECT is_bikeshop_admin 
            FROM users WHERE user_id = :id");
        $query->bindParam(':id' , $userId);
        $execute = $query->execute();
        $result = $query -> fetchAll(\PDO::FETCH_ASSOC);

        if(count($result) == 1){
            return $this -> getUserTypeFromResult($result[0]);
        }
        else
            return null;
    }

    /****************************************/

    // is user admin
    function isAdmin($userId){
        $conn = $this -> database -> getMySQLConnection();

        $query = $conn->prepare("SELECT is_admin 
            FROM users WHERE user_id = :id");
        $query->bindParam(':id' , $userId);
        $execute = $query->execute();
        $result = $query -> fetchAll(\PDO::FETCH_ASSOC);

        if(count($result) == 1){
            return $this -> getUserTypeFromResult($result[0]);
        }
        else
            return null;
    }

    /****************************************/

    // get user by mail
    function getUserByEmail($mail){
        $conn = $this -> database -> getMySQLConnection();

        $query = $conn->prepare("SELECT * FROM users WHERE user_mail = :mail");
        $query->bindParam(':mail' , $mail);
        $execute = $query->execute();
        $result = $query -> fetchAll(\PDO::FETCH_ASSOC);

        if(count($result) == 1)
            return $this -> getUserFromResult($result[0]);
        else
            return null;
    }

    /****************************************/

    // get users
    function getUsers() {
        $conn = $this -> database -> getMySQLConnection();

        $query = $conn->prepare("SELECT * FROM users");
        $execute = $query->execute();
        $result = $query -> fetchAll();
        $users = [];

        foreach ($result as $row) {
            array_push($users, $this -> getUserFromResult($row));
        }

        return $users;
    }
   
    /****************************************/
    /*            UPDATE                    */
    /****************************************/

    // update user
    function updateUser($user){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare ("UPDATE users SET 
            user_mail = :mail, 
            user_name = :name, 
            user_img = :img, 
            is_bikeshop_admin = :bikeshop_admin, 
            login_attempt_count = :login_attempt_count, 
            last_login_attempt_time = CURRENT_TIMESTAMP, 
            last_user_agent = :user_agent, 
            user_password = :password 
            WHERE user_id = :id");
        $query->bindParam(':id' , $user-> userId);
        $query->bindParam(':mail' , $user-> userMail);
        $query->bindParam(':name' , $user->userName);
        $query->bindParam(':img' , $user->img);
        $query->bindParam(':bikeshop_admin' , $user->isBikeAdmin);
        $query->bindParam(':login_attempt_count' , $user->loginAttemptCount);
        $query->bindParam(':user_agent' , $user->userAgent);
        $query->bindParam(':password' , $user->userPassword);
        $result = $query->execute();
    }

    /****************************************/

    // update user
    function updateUserProfile($user){
        $conn = $this -> database -> getMySQLConnection();
        $query = $conn->prepare ("UPDATE users SET 
            user_mail = :mail, 
            user_name = :name, 
            user_password = :password 
            WHERE user_id = :id");
        $query->bindParam(':id' , $user-> userId);
        $query->bindParam(':mail' , $user-> userMail);
        $query->bindParam(':name' , $user->userName);
        $query->bindParam(':password' , $user->userPassword);
        $result = $query->execute();
        return null;
    }

    /****************************************/

    // delete user
    function deleteUser($userId){
        $conn = $this -> database -> getMySQLConnection();

        $query = $conn->prepare("DELETE FROM users WHERE user_id = :id");
        $query->bindParam(':id' , $userId);
        $execute = $query->execute();
    }
    
    /****************************************/
    /*           CONSTRUCT RESULT           */
    /****************************************/

    // get User From Result
    private function getUserFromResult($result) {
        $user = new User();
        $user -> userId = base64_encode($result['user_id']);
        $user -> userMail = $result['user_mail'];
        $user -> userPassword = $result['user_password'];
        $user -> userName = $result['user_name'];
        $user -> img = $result['user_img'];
        $user -> isBikeAdmin = $result['is_bikeshop_admin'];
        $user -> isAdmin = $result['is_admin'];
        $user -> loginAttemptCount = $result['login_attempt_count'];
        $user -> lastLoginAttemptTime = $result['last_login_attempt_time'];
        $user -> userAgent = $result['last_user_agent'];
        return $user;
    }

    /****************************************/

    // get User Type From Result
    private function getUserTypeFromResult($result) {
        $user = new User();
        $user -> isBikeAdmin = isset($result['is_bikeshop_admin']) ? $result['is_bikeshop_admin'] : null;
        $user -> isAdmin = isset($result['is_admin']) ? $result['is_admin'] : null;
        return $user;
    }

    /****************************************/
    
    // get User Pass From Result
    private function getUserPassFromResult($result){
        $user = new User();
        $user -> userPassword = $result['user_password'];
        return $user;
    }
}


