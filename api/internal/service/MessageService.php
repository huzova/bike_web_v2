<?php

require_once (dirname(__FILE__) . '/../repository/MessageRepository.php');
require_once (dirname(__FILE__) .'/../model/Message.php');
include 'UserService.php'; 

/****************************************/

class MessageService {
    private $messageRepository;
    private $userService;

    public static function getInstance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new MessageService();
        }
        return $inst;
    }

    private function __construct() {
        $this-> messageRepository = MessageRepository::getInstance();
        $this -> userService = UserService::getInstance();
    }

    /****************************************/

    // create msg
    function create ($userId, $message) {
        if ($userId != "" || $userId != null || $message != "" || $message != null){
            $userType = $this -> getUserType();
            if ($userType == 'admin' || $userType == 'user'){
                $this-> messageRepository -> create($userId, $message);
                return null;
            } else
                return "invalid user";
        } else
            return "invalid input";
    }
    
    /****************************************/
    
    // getMsgByUserId
    function getMsgByUserId ($userId) {
        return $this-> messageRepository -> getMsgByUserId($userId);
    }

    /****************************************/
    
    // get all Msgs
    function getMsgs () {
        $userType = $this -> getUserType();

        // display messages to admin and bikeAdmin
        if ($userType == 'admin' || $userType == 'bikeAdmin'){
            return $this-> messageRepository -> getMsgs();
        } else 
            return null;
    }

    /****************************************/

    function getUserType(){
        if (isset($_SESSION['userId'])){
            $userId = base64_decode($_SESSION['userId']);
            $isBikeAdminResult = $this -> userService  -> isBikeAdmin($userId);
            $isAdminResult = $this -> userService -> isAdmin($userId);
            $isBikeAdmin = $isBikeAdminResult  -> isBikeAdmin;
            $isAdmin =  $isAdminResult -> isAdmin;
            if ($isBikeAdmin)
                return 'bikeAdmin';
            if ($isAdmin)
                return 'admin';
            if (!$isBikeAdmin || !$isAdmin)
                return 'user';
        } else 
            return null;
    }
}