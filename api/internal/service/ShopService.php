<?php

require_once (dirname(__FILE__) . '/../repository/ShopRepository.php');
require_once (dirname(__FILE__) . '/../repository/FileRepository.php');
require_once (dirname(__FILE__) .'/../model/Shop.php');
require_once (dirname(__FILE__) .'/../model/User.php');
require_once (dirname(__FILE__) .'/../model/ShopCategories.php');
require_once (dirname(__FILE__) .'/../model/OpeningHours.php');
require_once (dirname(__FILE__) .'/../model/Review.php');
include 'UserService.php'; 
/****************************************/

class ShopService {

    private $shopRepository;
    private $fileRepository;
    private $userService;

    public static function getInstance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new ShopService();
        }
        return $inst;
    }

    private function __construct() {
        $this-> shopRepository = ShopRepository::getInstance();
        $this-> fileRepository = FileRepository::getInstance();
        $this -> userService = UserService::getInstance();
    }
    
    /****************************************/
    /*            SAVE                      */
    /****************************************/
    
    // save basics
    function create ($shop) {
        $shopId = $this-> shopRepository -> create($shop);
        return $shopId;
    }

    function createShopAll($shop, $shopCategories) {
        $shopId = $this-> shopRepository -> create($shop);
        $shop -> shopId = $shopId;
        $shopCategories -> shopId = $shopId;
        $this->shopRepository->saveShopOpeningHours($shop);
        $this->shopRepository->saveShopCategory($shopCategories);

        // if(isset($imageTmp)) {
        //     $newShopImagePath = $this->fileRepository->uploadShopImage($image, $imageTmp);
        //     $shop ->  images = [$newShopImagePath];
        //     $this->shopRepository->saveShopImages($shop);
        // }

        return null;
    }

    /****************************************/

    // saveShopCategory
    function saveShopCategory ($shop) {
        $this-> shopRepository -> saveShopCategory($shop);
        return null;
    }

    /****************************************/

    function saveShopReview ($review) {
        $this-> shopRepository -> saveShopReview($review);
        return null;
    }

    /****************************************/
    /*            UPDATE                    */
    /****************************************/

    // update basics
    function updateShopBasic ($shop) {
        $this-> shopRepository -> updateShopBasic($shop);
        return null;
    }

    /****************************************/
    
    // update shop - business focus
    function updateShopCategory ($shop) {
        $this-> shopRepository -> updateShopCategory($shop);
        return null;
    }

    /****************************************/
    /*            CLAIM                     */
    /****************************************/
    
    // claim Shop
    function claimShop ($shop_id, $user_id) {
        $userType = $this -> userService  -> isBikeAdmin($user_id);
        $isBikeAdmin = $userType  -> isBikeAdmin;
        if ($isBikeAdmin){
            return $this-> shopRepository -> claimShop($shop_id, $user_id);
        } else {
            return "invalid user type";
        }
    }

    /****************************************/
    
    // unclaim Shop
    function unclaimShop ($shop_id, $user_id) {
        $this-> shopRepository -> unclaimShop($shop_id, $user_id);
        return null;
    }
    
    /****************************************/
    /*              GET                     */
    /****************************************/

    // get All Shop Basic Categories
    function getAllShopsBasicCategories ($is_approved) {
        // is admin ?
        $userId = base64_decode($_SESSION['userId']);
        $checkUserType = $this -> userService -> isAdmin($userId);
        $isAdmin =  $checkUserType -> isAdmin;
        
        if ($isAdmin){
            return $this-> shopRepository -> getAllShopsBasicCategories ($is_approved);
        } else {
            return null;
        }
    }
    
    /****************************************/
    
    function getAllShopsNearby ($lon, $lat, $radius) {
        return $this-> shopRepository -> findShopsNearby ($lon, $lat, $radius);
    }

    /****************************************/

    // get Shop Basics and Categories By shopId
    function getOpenningHoursByShopId ($shop_id) {
        return $this-> shopRepository -> getOpenningHoursByShopId ($shop_id);
    }
    
    /****************************************/

    // get Shop Basics and Categories By shopId
    function getShopBasicCategoriesById ($shop_id) {
        // is Bike Admin ?
        if (isset($_SESSION['userId'])){
            $userId = base64_decode($_SESSION['userId']);
            $userType = $this -> userService  -> isBikeAdmin($userId);
            $isBikeAdmin = $userType  -> isBikeAdmin;
        } else {
            $isBikeAdmin = false;
        }
        
        $dataOrError = $this-> shopRepository -> getShopBasicCategoriesById ($shop_id);
        
        if(is_object($dataOrError)) {
            // if user is shop admin only
            if ($isBikeAdmin) {
                $securityToken = uniqid();
                $_SESSION['securityToken-claimShop'] = $securityToken;
                $dataOrError -> securityToken = $securityToken;
            } else {
                $securityToken = uniqid();
                $_SESSION['securityToken-review'] = $securityToken;
                $dataOrError -> securityToken = $securityToken;
            }
        }
        return $dataOrError;
    }

    /****************************************/

    // get Admins By ShopId
    function getAdminsByShopId ($shop_id) {
        return $this-> shopRepository -> getAdminsByShopId ($shop_id);
    }

    /****************************************/

    // get Shops By UserId
    function getShopsByUserId ($user_id) {
        return $this-> shopRepository -> getShopsByUserId ($user_id);
    }

    /****************************************/

    // get Reviews By ShopId
    function getReviewsByShopId ($shop_id) {
        return $this-> shopRepository -> getReviewsByShopId ($shop_id);
    }

    /****************************************/

    // has Review Instance
    function hasReviewInstance ($shop_id, $user_id) {
        return $this-> shopRepository -> hasReviewInstance ($shop_id, $user_id);
    }

    /****************************************/

    // get rating By ShopId
    function getAvgRatingByShopId ($shop_id) {
        return $this-> shopRepository -> getAvgShopRating ($shop_id);
    }

    /****************************************/
    
     // getNotApprovedShops
     function getNotApprovedShops () {
        return $this-> shopRepository -> getNotApprovedShops ();
    }

    /****************************************/
    /*              DELETE                  */
    /****************************************/

    // delete shop by id 
    function deleteByShopId ($shop_id) {
        // is admin ?
        $userId = base64_decode($_SESSION['userId']);
        $checkUserType = $this -> userService -> isAdmin($userId);
        $isAdmin =  $checkUserType -> isAdmin;
        if ($isAdmin){
            return $this-> shopRepository -> deleteByShopId ($shop_id);
        } else{
            return "err - user type";
        }
    }

    /****************************************/
    /*              APPROVE                 */
    /****************************************/

    // approve Shop
    function approveShop ($shop_id) {
        // is admin ?
        $userId = base64_decode($_SESSION['userId']);
        $checkUserType = $this -> userService -> isAdmin($userId);
        $isAdmin =  $checkUserType -> isAdmin;
        if ($isAdmin){
            return $this-> shopRepository -> approveShop ($shop_id);
        } else{
            return "err - user type";
        }
    }

    /****************************************/
    /*              OTHER                   */
    /****************************************/

    // push Opening Hours
    function pushOpeningHours($shopData){
        $shortShopData = array_slice($shopData, 0, 1);
        for ($i=0; $i < count($shortShopData); $i++) { 
            $shopObject = $shortShopData[$i];
            $shopId = $shopObject -> shopId;
            $oH = $this -> getOpenningHoursByShopId($shopId);
            if($oH != null){
                $shopObject -> openingHours = $oH;
            }
        }return $shortShopData;
    }

    

}

