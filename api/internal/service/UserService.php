<?php
// requre files
require_once (dirname(__FILE__) . '/../repository/UserRepository.php');
require_once (dirname(__FILE__) . '/../repository/FileRepository.php');
require_once (dirname(__FILE__) .'/../model/User.php');

/* ************************************************* */

class UserService {
    // proprties
    private $userRepository;
    private $fileRepository;
    private $pepper = "ThisIsMySecretPeper";

    /* ************************************************* */

    // get user service 
    public static function getInstance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new UserService();
        }
        return $inst;
    }

    // nainicializujeme dalsie repozitory ktore chceme pouzivat
    private function __construct() {
        $this-> userRepository = UserRepository::getInstance();
        $this-> fileRepository = FileRepository::getInstance();
    }

    /* ************************************************* */
    
    // Sign up
    function create ($user) {
        $containsNumber = preg_match('/\\d/', $user -> userPassword) > 0; // = true / false
        $containsUpperCase = preg_match("/[A-Z]/", $user -> userPassword) > 0;
        $containsLowerCase = preg_match("/[a-z]/", $user -> userPassword) > 0;

        if (filter_var($user -> userMail, FILTER_VALIDATE_EMAIL) && strlen($user -> userPassword) >= 8 && // filter_var - check valid mail | strlen - check pass length
            $containsNumber && $containsUpperCase &&
            $containsLowerCase && $user -> userName != null && $user -> userName != "") {
            
                // hash password
                $options = Array("cost"=>11);
                $user -> userPassword = password_hash($user -> userPassword.$this -> pepper, PASSWORD_DEFAULT, $options);
                
                // check if user exist
                $userByEmail = $this -> userRepository -> getUserByEmail($user -> userMail);

                // save user and img
                if($userByEmail == null) {

                    //  upload user image if user has set img
                    if(isset($user-> imgTmp)) {
                        // upload file and get a new file path
                        $this->fileRepository->uploadUserProfile($user);
                    }

                    // create user in database
                    try {
                        $this->userRepository->createUser($user);
                        $userId = $user -> userId;
                        $_SESSION['userId'] = base64_encode($userId);
                        $_SESSION['token'] = uniqid();
                        return $user;
                    } catch  (Exception $e) {
                        return $e-> getMessage();
                    }

                } else {
                    return 'User already exists';
                }

        } else {
            return 'Can\'t create user';
        }
    }

    /* ************************************************* */

    // Login

    // Returns either:
    //    - user object on successful login
    //    - error array when something went wrong

    function login($email, $password) {

        // validate user input
        if($email == null || $email == "")
            // Empty email
            return ['error' => "Err", 'timeout' => false];
        if($password == null || $password == "")
            // Empty password
            return ['error' => "Err", 'timeout' => false];

        if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            // Email not valid
            return ['error' => "Err", 'timeout' => false];
            
        // check if user exists
        $user = $this->userRepository -> getUserByEmail($email);

        if($user == null)
            // User not found
            return ['error' => "Err", 'timeout' => false];

        $user -> userId = base64_decode($user -> userId); 
        // check currentDateTime dbDateTime timeDifference
        $currentDateTime = new DateTime();
        $dbDateTime = new DateTime($user->lastLoginAttemptTime);
        $timeDifference = $currentDateTime->getTimestamp() - $dbDateTime->getTimestamp();

        if($timeDifference < 300 && $user->loginAttemptCount >= 3) {
            $user -> loginAttemptCount = $user -> loginAttemptCount + 1;
            $this -> userRepository -> updateUser($user);
            // Blocked
            return ['error' => "Err", 'timeout' => true];
        }

        // check password
        if (password_verify($password.$this -> pepper, $user->userPassword) == false) {
            $user -> loginAttemptCount = $user -> loginAttemptCount + 1;
            
            $this -> userRepository -> updateUser($user);
            // Wrong password
            return ['error' => "Err", 'timeout' => false];
        }

        // all good - set session, reset login attempt count
        $user -> loginAttemptCount = 0;
        $this->userRepository -> updateUser($user);

        $user -> userId = base64_encode($user -> userId);
        $_SESSION['userId'] = $user->userId;
        $_SESSION['token'] = uniqid();

        return $user;
    }

    /* ************************************************* */

    // function check pass 
    function isValidPass($password){
        $containsNumber = preg_match('/\\d/', $password) > 0; // = true / false
        $containsUpperCase = preg_match("/[A-Z]/", $password) > 0;
        $containsLowerCase = preg_match("/[a-z]/", $password) > 0;
        $correctLength = strlen($password) >= 8;
        return $correctLength && $containsNumber && $containsUpperCase && $containsLowerCase;
    }
    /* ************************************************* */

    // Update user profile
    function updateUserProfile ($user) {
       return  $this->userRepository -> updateUserProfile($user);
    }

    /* ************************************************* */
    
    // Delete user
    function delete ($user) {
        $this->userRepository -> deleteUser($user -> userId);
        // $this->fileRepository -> delete($user -> img);
    }

    /* ************************************************* */
    
    // Get user by email
    function getByEmail ($email) {
        return $this -> userRepository -> getUserByEmail($email);
    }

    /* ************************************************* */
    
    // Get user by id
    function getById ($id) {
        return $this -> userRepository -> getUserById($id);
    }

    /* ************************************************* */
    
    // is bike admin 
    function isBikeAdmin($userId) {
        return $this -> userRepository -> isBikeAdmin($userId);
    }

    /* ************************************************* */
    
    // is admin 
    function isAdmin($userId) {
        return $this -> userRepository -> isAdmin($userId);
    }
    
    /* ************************************************* */
    
    // Get users
    function getAll () {
        return $this -> userRepository -> getUsers();
    }

    /* ************************************************* */

    // getUserBy Id
    function getUserPass($userId) {
        return $this -> userRepository -> getUserPass($userId);
    }
}

