<?php

// if session is not writable
if (!is_writable(session_save_path())) {
    // get document server root folder (bikeweb)
    $path = realpath(dirname($_SERVER['DOCUMENT_ROOT']));
    // append /session to server root
    $path .= '/session';

    // if session directory doesn't exist, create it
    if(!file_exists($path)) {
        mkdir($path, 0700, true);
    }

    // set document root folder as session path
    ini_set('session.save_path', $path);

    // now check again if session path is set
    if (!is_writable(session_save_path())) {
        // if not then print out error so it's clear that it's not working
        echo 'Session path "'.session_save_path().'" is not writable for PHP!';
    }

}

// set http only cookie
ini_set( 'session.cookie_httponly', 1 );

// if session is set, check if session is already started
if (session_status() == PHP_SESSION_NONE) {
    // if session is not started, then start the session
    session_start();
}
// allow cross-origin requests from angular app running on localhost:4200 + allow required headers
header("Access-Control-Allow-Origin: https://localhost:4200");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Expose-Headers: *");
header("Access-Control-Max-Age: 86400");
// set response format as json with utf8 encoding
header("Content-Type: application/json; charset=UTF-8");
