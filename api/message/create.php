<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/MessageService.php'; 

    /****************************************/
    try{
        $messageService = MessageService::getInstance();
        if (isset($_SESSION['userId'])){
            $userId = base64_decode($_SESSION['userId']);
        } else {
            echo '{"err":true, "ok":false}';
            return;
        }

        $message = $_POST['message'];
        $response = $messageService -> create($userId, $message);

        if(is_string($response)) {
            echo '{"err":true, "ok":false, "status":"'. $response.'"}';
        } else {
            echo '{"err":false, "ok":true}';
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false}';
    }