<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/MessageService.php'; 

    /****************************************/
    try{
        $messageService = MessageService::getInstance();
        $response = $messageService -> getMsgs();

        if($response != null){
            echo '{"err":false, "data": '.json_encode($response).'}';
        } else 
            echo '{"status": "err", "ok":false }';
            
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false }';
    }