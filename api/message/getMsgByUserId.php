<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/MessageService.php'; 

    /****************************************/

    try{
        $messageService = MessageService::getInstance();
        $userId = $_POST['userId'];

        $response = $messageService -> getMsgByUserId($userId);
        
        echo '{"err":false, "data": '.json_encode($response).'}';
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }