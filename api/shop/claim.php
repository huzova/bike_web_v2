<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/ShopService.php'; 

    /****************************************/

    try{
        // security token
        $token = $_POST['securityToken'];
        $expectedToken = $_SESSION['securityToken-claimShop'];
        
        // claim shop
        if($token == $expectedToken) {
            $shopService = ShopService::getInstance();
            $shopId = base64_decode($_POST['shopId']);
            $userId = base64_decode($_SESSION['userId']);

            $response = $shopService -> claimShop($shopId, $userId);

            if(is_string($response)) {
                echo '{"err":true, "ok":false, "status":"'. $response.'"}';
            } else {
                echo '{"err":false, "ok":true}';
            }
        } else {
            echo '{"err":true, "ok":false, "status": "Invalid security token"}';
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }