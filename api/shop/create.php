<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/ShopService.php'; 
    
    /****************************************/
    
    try{
        // check address   
        if (!isset($_POST['longitude']) || !isset($_POST['latitude'])){
            echo '{"err":true, "ok":false, "status": "Invalid address"}';
            exit;
        }
        
        /****************************************/

        //create shop obj
        $shopService = ShopService::getInstance();
        $shopObject = new Shop();
        $shopObject -> shopName = $_POST['shopName'];
        $shopObject -> shopAddress = $_POST['shopAddress'];
        $shopObject -> shopPhone = $_POST['shopPhone'];
        $shopObject -> pricePoint = $_POST['pricePoint'];
        $shopObject -> longitude = $_POST['longitude'];
        $shopObject -> latitude = $_POST['latitude'];

        $shopObject->openingHours = [
            processOpeningHour('Monday'),
            processOpeningHour('Tuesday'),
            processOpeningHour('Wednesday'),
            processOpeningHour('Thursday'),
            processOpeningHour('Friday'),
            processOpeningHour('Saturday'),
            processOpeningHour('Sunday')
        ];

        $categories = new ShopCategories();
        $categories -> sellsNewBikes = processCategory('isNewBikeSeller');
        $categories -> sells2ndHandBikes = processCategory('is2ndHandBikeSeller');
        $categories -> isRental = processCategory('isBikeRental');
        $categories -> isBikeRepair = processCategory('isBikeRepair');
        $categories -> sellsAccessories = processCategory('isAccessoriesSeller');

        // $img = isset( $_FILES['shopImage']['name']) ?  $_FILES['shopImage']['name'] : null;
        // $imgTmp = isset( $_FILES['shopImage']['tmp_name']) ?  $_FILES['shopImage']['tmp_name'] : null;

        $response = $shopService -> createShopAll($shopObject, $categories);

        if(is_string($response)) {
            echo '{"err":true, "ok":false, "status":"'. $response.'"}';
        } else {
            echo '{"err":false, "ok":true}';
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }
    
    /****************************************/

    function processCategory($tag) {
        if(isset($_POST[$tag])) {
            $value = $_POST[$tag];
            if(is_string($value) && ($value === "true" || $value === 'true') )
                return 1;
            else
                return 0;
        } else {
            return 0;
        }
    }

    /****************************************/

    function processOpeningHour($tag) {
        $openingHours = new OpeningHours();
        $openingHours -> day = $tag;
        $openingHours -> openTime = processOpeningHourSingle($tag . "Open");
        $openingHours -> closeTime = processOpeningHourSingle($tag . "Close");
        return $openingHours;
    }

    /****************************************/

    function processOpeningHourSingle($tag) {
        if(isset($_POST[$tag])) {
            $value = $_POST[$tag];
            if(empty($value) || $value == '')
                return -1;
            return 100 * intval($value);
        } else {
            return -1;
        }
    }