<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/ShopService.php'; 

    /****************************************/

    try{
        $shopService = ShopService::getInstance();
        $shopId = base64_decode($_POST['shopId']);

        $response = $shopService -> deleteByShopId($shopId);
        
        if(is_string($response)) {
            echo '{"err":true, "ok":false, "status":"'. $response.'"}';
        } else {
            echo '{"err":false, "ok":true}';
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }
