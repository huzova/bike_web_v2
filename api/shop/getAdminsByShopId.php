<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/ShopService.php'; 

    /****************************************/

    try{
        $shopService = ShopService::getInstance();
        $shopId = base64_decode($_POST['shopId']);

        $response = $shopService -> getAdminsByShopId($shopId);

        if($response == null || is_string($response)) {
            echo '{"err":true, "ok":false, "status":"'. $response.'"}';
        } else {
            echo '{"err":false, "ok":true, "data": '.json_encode($response).'}';
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }