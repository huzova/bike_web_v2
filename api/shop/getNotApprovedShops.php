<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/ShopService.php'; 

    /****************************************/

    try{
        $shopService = ShopService::getInstance();

        $response = $shopService -> getNotApprovedShops();

        if($response != null) {
            echo $response;
        } else {
            echo null;
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }