<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/ShopService.php'; 

    /****************************************/

    try{
        $shopService = ShopService::getInstance();
        $shopId = base64_decode($_POST['shopId']);

        $reviews = $shopService -> getReviewsByShopId($shopId);
        $rating = $shopService -> getAvgRatingByShopId($shopId);

        if($reviews == null || $rating == null || is_string($reviews)) {
            echo '{"err":true, "ok":false, "status":"'. $reviews.'"}';
        } else {
            echo '{"err":false, "ok":true, "data": '.json_encode($reviews).', "rating": '.$rating.'}';
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }