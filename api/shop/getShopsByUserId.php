<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/ShopService.php'; 

    /****************************************/
    
    try{
        $shopService = ShopService::getInstance();
        $userId = $_SESSION['userId'];

        $response = $shopService -> getShopsByUserId($userId);

        if(is_string($response)) {
            echo '{"err":true, "ok":false, "status":"'. $response.'"}';
        } else {
            echo '{"err":false, "ok":true, "data": '.json_encode($response).'}';
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }
