<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/ShopService.php';

    /****************************************/

    try{
        $shopService = ShopService::getInstance();
        
        $is_approved = 0;
        $response = $shopService -> getAllShopsBasicCategories($is_approved);

        if(is_string($response)) {
            echo '{"err":true, "ok":false, "status":"'. $response.'"}';
        } else {
            // push O.H
            $newResponse = $shopService -> pushOpeningHours($response);
            echo '{"err":false, "ok":true, "data": '.json_encode($newResponse).'}';
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }
      