<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/ShopService.php'; 

    /****************************************/

    try{
        // security token
        $token = $_POST['securityToken'];
        $expectedToken = $_SESSION['securityToken-review'];
        // has token = NOT bikeadmin 
        
        /****************************************/
        
        // save Shop Review
        if($token == $expectedToken) {
            $shopService = ShopService::getInstance();
            $review = new Review;
            $review -> shopId = base64_decode($_POST['shopId']);
            $review -> userId = $_SESSION['userId'];
            $review -> rating = $_POST['rating'];
            $review -> review = $_POST['review'];

            $hasReviewInstance = $shopService -> hasReviewInstance($review -> shopId, $review -> userId);
            if ($hasReviewInstance){
                echo '{"err":true, "ok":false, "message": "Review already written"}';
                exit;
            }

            $response = $shopService -> saveShopReview($review);

            if(is_string($response)) {
                echo '{"err":true, "ok":false, "status":"'. $response.'"}';
            } else {
                echo '{"err":false, "ok":true}';
            }
        } else {
            echo '{"err":true, "ok":false, "status": "Invalid security token"}';
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }
