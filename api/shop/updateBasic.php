<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/ShopService.php'; 

    /****************************************/
    try{
        $shopService = ShopService::getInstance();
        
        $shopObject = new Shop();
        $shopObject -> shopId = base64_decode($_POST['shopId']);
        $shopObject -> shopName = $_POST['shopName'];
        $shopObject -> shopAdress = $_POST['shopAdress'];
        $shopObject -> shopPhone = $_POST['shopPhone'];
        $shopObject -> pricePoint = $_POST['pricePoint'];

        $response = $shopService -> updateShopBasic($shopObject);

        if($response == null || is_string($response)) {
            echo '{"err":true, "ok":false, "status":"'. $response.'"}';
        } else {
            echo '{"err":false, "ok":true, "data": '.json_encode($response).'}';
        }
        
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }