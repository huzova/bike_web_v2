<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/ShopService.php'; 

    /****************************************/

    try{
        $shopService = ShopService::getInstance();
        $shopObject = new ShopCategories();
        $shopObject -> shopId = base64_decode($_POST['shopId']);
        $shopObject -> sellsNewBikes = $_POST['sellsNewBikes'] === 'true' ? true : false ;
        $shopObject -> sells2ndHandBikes = $_POST['sells2ndHandBikes'] === 'true' ? true : false ;
        $shopObject -> isRental = $_POST['isRental'] === 'true' ? true : false ;
        $shopObject -> isBikeRepair = $_POST['isBikeRepair'] === 'true' ? true : false ;
        $shopObject -> sellsAccessories = $_POST['sellsAccessories'] === 'true' ? true : false ;

        $response = $shopService -> updateShopCategory($shopObject);

        if($response == null || is_string($response)) {
            echo '{"err":true, "ok":false, "status":"'. $response.'"}';
        } else {
            echo '{"err":false, "ok":true, "data": '.json_encode($response).'}';
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }
   
