<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/UserService.php';
    
    /****************************************/
    
    try{
        $userService = UserService::getInstance();
        $userObject = new User();
        $userObject -> userMail = $userMail = $_POST['userMail'];
        $userObject -> userPassword = $_POST['userPassword'];
        $userObject -> userName = $_POST['userName'];
        $userObject -> isBikeAdmin = $_POST['isBikeAdmin'] === "true";
        if(!empty($_SERVER['HTTP_USER_AGENT']))
            $userObject -> userAgent = $_SERVER['HTTP_USER_AGENT'];
        if(!empty($_FILES['userImg']['name'])) {
            $userObject -> img = $_FILES['userImg']['name'];
            $userObject -> imgTmp = $_FILES['userImg']['tmp_name'];
        }

        $response = $userService -> create($userObject);

        if($response == null || is_string($response)) {
            echo '{"err":true, "ok":false, "status":"'. $response.'"}';
        } else { 
            // is user, don't display following
            unset($response -> userPassword);
            unset($response -> userAgent);
            unset($response -> loginAttemptCount);
            unset($response -> lastLoginAttemptTime);
            echo '{"err":false, "ok":true, "data": '.json_encode($response).'}';
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }
