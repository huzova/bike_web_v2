<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/UserService.php';
    
    /****************************************/

    try{
        // security token
        $token =  $_POST['token'];
        $expectedToken = $_SESSION['token'];
        
        // new user object
        $user = new User();
        $user -> userId = base64_decode($_SESSION['userId']);

        // call service
        if($token == $expectedToken) {
            $userService = UserService::getInstance();
            $userService -> delete($user);
        } else {
            echo '{"err":true, "status": "Invalid token"}';
        }
        
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }

