<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/UserService.php';
    
    /* ************************************************* */

    try{
        //if post user id is set, use that one (e.g. when admin user tries to fetch different user by id
        if (isset($_POST['userId'])){
            $userId = $_POST['userId'];
        }
        
        /* ************************************************* */
        
        // echo 'sessoin id ' . session_id();
        // if userid is not set then try to fetch user by session id

        if(!isset($userId)) {
            if(isset($_SESSION ['userId'])) {
                $userId  = base64_decode($_SESSION ['userId']);
            } else {
                echo '{"err":true, "status": "Invalid session id"}';
                exit;
            }
        }

        /* ************************************************* */

        $userService = UserService::getInstance();
        $response = $userService -> getById($userId);
        unset($response -> userPassword);
        unset($response -> userAgent);
        unset($response -> loginAttemptCount);
        unset($response -> lastLoginAttemptTime);
        echo '{"err":false, "data": '.json_encode($response).'}';
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }