<?php
    // includes
    include '../internal/session.php';
    include '../internal/service/UserService.php';
    
    /* ************************************************* */
    
    try{
        $userId = base64_decode($_SESSION['userId']);

        $userPassword = isset($_POST['userPassword']) ? $_POST['userPassword'] : false;
        $newUserPassword = isset($_POST['newUserPassword']) ? $_POST['newUserPassword'] : null;

        /* ************************************************* */

        // get user pass 
        $userService = UserService::getInstance();
        $userPassObj = $userService -> getUserPass($userId);
        $userPassDB = $userPassObj  -> userPassword;

        $userObject = new User();
        $userObject -> userId = $userId;

        if ( isset($_POST['userName']) && isset($_POST['userMail']) && 
            filter_var($_POST['userMail'], FILTER_VALIDATE_EMAIL)){
            $userObject -> userMail = $_POST['userMail'];
            $userObject -> userName = $_POST['userName'];
        } else {
            echo '{"err":true, "ok":false, "status": "Invalid input"}';
            exit;
        }
        
        /* ************************************************* */
        
        if (!$userPassword){
            $userObject -> userPassword = $userPassDB;
            $response = $userService -> updateUserProfile($userObject);
        } else {
            if (password_verify($userPassword.'ThisIsMySecretPeper', $userPassDB) && 
                $userService -> isValidPass($newUserPassword)){
                    $newUserPasswordHash = password_hash($newUserPassword.'ThisIsMySecretPeper', PASSWORD_DEFAULT, Array("cost"=>11));
                    $userObject -> userPassword = $newUserPasswordHash;
                    $response = $userService -> updateUserProfile($userObject);
            } else {
                echo '{"err":true, "ok":false, "status": "Invalid password"}';
            }
        }
    } catch (Exception $e) {
        echo '{"status": "err", "ok":false, "status":"err"}';
    }
        

    