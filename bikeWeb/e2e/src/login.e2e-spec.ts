import {browser, by, element} from 'protractor';

// to run the test => ng e2e
describe('TODO list', function() {
  it('should login', function() {
    browser.get('/login');
    // element.all(by.tagName('button')).get(5).click();

    const userMail = element(by.css('input[formControlName=userMail]'));
    userMail.sendKeys('a@a.com');
    expect(userMail.getAttribute('value')).toBe('a@a.com');

    const userPassword = element(by.css('input[formControlName=userPassword]'));
    userPassword.sendKeys('Aa111111111');
    expect(userPassword.getAttribute('value')).toBe('Aa111111111');

    element.all(by.tagName('button')).get(5).click();

    browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        console.log('Url', url);
        return url;
      });
    }, 5000, 'URL hasn\'t changed');
  });
});
