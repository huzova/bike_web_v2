import {browser, by, element, protractor} from 'protractor';

// to run the test => ng e2e
describe('Login test', function() {

  it('should register', function() {

    // create new profile
    browser.get('/register');
    const userEmail = 'xxx' + new Date().getTime() + '@a.com';
    const userMailField = element(by.css('input[formControlName=userMail]'));
    userMailField.sendKeys(userEmail);
    expect(userMailField.getAttribute('value')).toBe(userEmail);

    const userNameField = element(by.css('input[formControlName=userName]'));
    userNameField.sendKeys(userEmail);
    expect(userNameField.getAttribute('value')).toBe(userEmail);

    const userPasswordField = element(by.css('input[formControlName=userPassword]'));
    const password = 'Aa111111111';
    userPasswordField.sendKeys(password);
    expect(userPasswordField.getAttribute('value')).toBe(password);

    element.all(by.id('submitButton')).get(0).click();

    var EC = protractor.ExpectedConditions;
    browser.wait(EC.urlContains('map'), 5000);
  });
});
