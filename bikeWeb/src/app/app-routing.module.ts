import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { MapComponent } from './components/map/map.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { CreateBikeShopComponent } from './components/create-bike-shop/create-bike-shop.component';
import { BikeShopDetailsComponent } from './components/bike-shop-details/bike-shop-details.component';
import { ShopsToApproveComponent } from './components/admin-dashboard/shops-to-approve/shops-to-approve.component';
import { UsersComponent } from './components/admin-dashboard/users/users.component';
import { OffersComponent } from './components/offers/offers.component';
import { ShopsComponent } from './components/admin-dashboard/shops/shops.component';
import { EditShopComponent } from './components/admin-dashboard/shops/edit-shop/edit-shop.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const routes: Routes = [
  {path: '', component: MapComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'map', component: MapComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'admin-dashboard', component: AdminDashboardComponent},
  {path: 'shops-to-approve', component: ShopsToApproveComponent},
  {path: 'create-bike-shop', component: CreateBikeShopComponent},
  {path: 'bike-shop-details/:shopId', component: BikeShopDetailsComponent},
  {path: 'users', component: UsersComponent},
  {path: 'offers', component:OffersComponent},
  {path: 'shops', component:ShopsComponent},
  {path: 'edit-shop', component: EditShopComponent},
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
