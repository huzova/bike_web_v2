import { Component } from '@angular/core';
import { LoginService } from './services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bikeWeb';

  constructor(
    private loginService: LoginService,
    private router: Router) { }

  pageTiitle = this.router.url;

  onActivate(event) {
    window.scroll(0,0);
  }
}
