import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// angular material
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {MatCardModule} from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatDividerModule} from '@angular/material/divider';
import {MatSelectModule} from '@angular/material/select';
import {MatStepperModule} from '@angular/material/stepper';
import {MatChipsModule} from '@angular/material/chips';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatListModule} from '@angular/material/list';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { MapComponent } from './components/map/map.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './components/profile/profile.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { FormsModule } from '@angular/forms';
import { CreateBikeShopComponent } from './components/create-bike-shop/create-bike-shop.component';
import { BikeShopDetailsComponent } from './components/bike-shop-details/bike-shop-details.component';
import { ShopsToApproveComponent} from './components/admin-dashboard/shops-to-approve/shops-to-approve.component';
import { LoginService } from './services/login.service';
import { TimeService } from './services/timeData/time.service';
import { SelectDataService } from './services/selectData/select-data.service';
import { UsersDataService } from './services/usersData/users-data.service';
import { UsersComponent } from './components/admin-dashboard/users/users.component';
import { ShopsDataService } from './services/shopsData/shops-data.service';
import { OffersComponent } from './components/offers/offers.component';
import { ShopsComponent } from './components/admin-dashboard/shops/shops.component';
import { EditShopComponent } from './components/admin-dashboard/shops/edit-shop/edit-shop.component';
import {HttpClientModule} from "@angular/common/http";
import { StorageServiceModule } from "angular-webstorage-service";
import { LocalStorageService } from './services/localStorage/local-storage.service';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { GoogleAddressAutocompleteDirective } from './directives/google-address-autocomplete.directive';
import { FilterShopsPipe } from './pipes/filter-shops.pipe';
import { DragScrollModule } from 'ngx-drag-scroll';
import { SocialLoginModule, AuthServiceConfig, FacebookLoginProvider} from "angular-6-social-login-v2";

// Configs 
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("134936034124419")
        }
      ]
    );
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    MapComponent,
    ProfileComponent,
    AdminDashboardComponent,
    CreateBikeShopComponent,
    BikeShopDetailsComponent,
    ShopsToApproveComponent,
    UsersComponent,
    OffersComponent,
    ShopsComponent,
    EditShopComponent,
    PageNotFoundComponent,
    GoogleAddressAutocompleteDirective,
    FilterShopsPipe
  ],
  imports: [
    // angular material
    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatExpansionModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatSelectModule,
    MatStepperModule,
    MatChipsModule,
    MatGridListModule,
    MatCheckboxModule,
    MatListModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatTableModule,
    
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    StorageServiceModule,
    DragScrollModule,
    SocialLoginModule,
  ],
  providers: [
    LoginService,
    TimeService,
    SelectDataService,
    UsersDataService,
    ShopsDataService,
    LocalStorageService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ],
  bootstrap: [AppComponent]
})

 
export class AppModule { }
