import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { UsersDataService } from 'src/app/services/usersData/users-data.service';
import { ShopsDataService } from 'src/app/services/shopsData/shops-data.service';
import { ShopService } from 'src/app/services/api/shop/shop.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  // properties
  notApprovedShops : number;

  constructor(
    private router: Router,
    private loginService: LoginService, 
    private usersService: UsersDataService,
    private shopsService: ShopsDataService,
    private api: ShopService) { }
  
  ngOnInit() { 
    this.getNotApprovedShops();
   }

  // countData(){
  //   this.usersCount = this.usersService.usersData.length;
  //   this.shopsCount = this.shopsService.bikeShopData.length;
  // }

  getNotApprovedShops(){
    this.api.getNotApprovedShops()
    .subscribe((response) => {
      if (response != null)
        this.notApprovedShops = response;
    })
  }
}
