import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopsToApproveComponent } from './shops-to-approve.component';

describe('ShopsToApproveComponent', () => {
  let component: ShopsToApproveComponent;
  let fixture: ComponentFixture<ShopsToApproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopsToApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopsToApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
