import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { MatSnackBar } from '@angular/material';
import { TimeService } from 'src/app/services/timeData/time.service';
import { SelectDataService } from 'src/app/services/selectData/select-data.service';
import { ShopService } from 'src/app/services/api/shop/shop.service';

/****************************************/

@Component({
  selector: 'app-shops-to-approve',
  templateUrl: './shops-to-approve.component.html',
  styleUrls: ['./shops-to-approve.component.scss']
})

/****************************************/

export class ShopsToApproveComponent implements OnInit {
  // properties
  pendings : any = [];
  notApprovedShops : number;
  
  /****************************************/

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private loginService: LoginService,
    public snackBar: MatSnackBar,
    private timeService: TimeService,
    private api: ShopService, 
    private selectService: SelectDataService) 
    { }
  
  /****************************************/

  ngOnInit() {   
    this.getNotApprovedShops();
    this.getShops();
    // console.log("pendings ", this.pendings);
  }

  /****************************************/

  toHide(message) {
    this.openSnackBar(message, '');
    this.pendings.splice(0,1);
  }

  /****************************************/

  deleteShop (shopId){
    this.openSnackBar('place instance canceled!', '');
    
    let formData = new FormData();
    formData.append('shopId', shopId);
    
    this.api.delete(formData)
    .subscribe((response) => {
      if (response != null){
        // console.log('err', response);
        console.log('err');
      } else {
        this.getShops();
        this.getNotApprovedShops();
      }
    })

  }

  /****************************************/

  approveShop(shopId){
    this.openSnackBar('place published!', '');

    let formData = new FormData();
    formData.append('shopId', shopId);

    this.api.approve(formData)
    .subscribe((response) => {
      if (response != null){
        // console.log('err', response);
        console.log('err');
      } else {
        this.getShops();
        this.getNotApprovedShops();
      }
    })
  }
  /****************************************/
  
  getNotApprovedShops(){
    this.api.getNotApprovedShops()
    .subscribe((response) => {
      if (response != null)
        this.notApprovedShops = response;
    })
  }

  /****************************************/

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  /****************************************/

  getShops(){
    this.api.getShopsToApprove()
    .subscribe((response) => {
      this.pendings = response;
      // console.log("this.pendings ", this.pendings);
      // this.constructForm();
    })
  }

  /****************************************/

  // constructForm(){
  //   for (let i = 0; i < this.pendings.length; i++) {
  //     const pending = this.pendings[i];
  //     console.log("pending ", pending.openingHours);  
  //     // this.constructFirstFormGroup(pending);
  //     // this.constructThirdFormGroup(pending);
  //     // this.constructThirdFormGroup(pending);
  //   }
  // }

}
