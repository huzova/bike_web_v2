import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { MatSnackBar } from '@angular/material';
import { TimeService } from 'src/app/services/timeData/time.service';
import { SelectDataService } from 'src/app/services/selectData/select-data.service';

@Component({
  selector: 'app-edit-shop',
  templateUrl: './edit-shop.component.html',
  styleUrls: ['./edit-shop.component.scss']
})
export class EditShopComponent implements OnInit {
  // properties
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;

  times: any; // [];
  days: any; // [];
  pricePoints: any; // [];

  bikeShop = {
    "bikeShopName": "a", 
    "bikeShopAddress":"Flydedokken 7, 2450, Copenhagen", 
    "bikeShopPhone": 12345678, 
    "bikeShopPricePoint": "$$-1"
  };

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private loginService: LoginService,
    public snackBar: MatSnackBar,
    private timeService: TimeService,
    private selectService: SelectDataService) {
      // validate firstFormGroup
      this.firstFormGroup = this.fb.group({
        bikeShopName: ['', Validators.required],
        bikeShopAddress: ['', Validators.required],
        bikeShopPhone: ['', Validators.required],
        bikeShopPricePoint: ['', Validators.required]
      });
      // validate secondFormGroup
      this.secondFormGroup = this.fb.group({
        isNewBikeSeller: [''],
        is2ndHandBikeSeller: [''],
        isBikeRental: [''],
        isBikeRepair: [''],
        isAccessoriesSeller: ['']
      });
      // validate thirdFormGroup
      this.thirdFormGroup = this.fb.group({
        MondayOpen: [''],
        MondayClose: [''],
        TuesdayOpen: [''],
        TuesdayClose: [''],
        WednesdayOpen: [''],
        WednesdayClose: [''],
        ThursdayOpen: [''],
        ThursdayClose: [''],
        FridayOpen: [''],
        FridayClose: [''],
        SaturdayOpen: [''],
        SaturdayClose: [''],
        SundayOpen: [''],
        SundayClose: ['']
      });
      // validate fourthFormGroup
      this.fourthFormGroup = this.fb.group({
        shopImage: [''],
      });

    }

  ngOnInit() {
     // prefill forms
     this.constructFirstFormGroup(this.bikeShop);
    
     // get data from services
     this.times = this.timeService.timeData;
     this.days = this.timeService.dayData;
     this.pricePoints = this.selectService.pricePointData;
  }

  constructFirstFormGroup(bikeShop) {
    this.firstFormGroup = this.fb.group({
      bikeShopName: [bikeShop == null ? '' : bikeShop.bikeShopName, Validators.required],
      bikeShopAddress: [bikeShop == null ? '' : bikeShop.bikeShopAddress, Validators.required],
      bikeShopPhone: [bikeShop == null ? '' : bikeShop.bikeShopPhone, Validators.required],
      bikeShopPricePoint: [bikeShop == null ? '' : bikeShop.bikeShopPricePoint, Validators.required]
    });
  }

  redirect(){
    this.router.navigate(['/shops']);
  }

}
