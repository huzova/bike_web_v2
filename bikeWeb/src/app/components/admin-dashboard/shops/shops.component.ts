import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { ShopsDataService } from 'src/app/services/shopsData/shops-data.service';
import { UsersDataService } from 'src/app/services/usersData/users-data.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { ShopService } from 'src/app/services/api/shop/shop.service';

@Component({
  selector: 'app-shops',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.scss']
})
export class ShopsComponent implements OnInit {
  shops: any;
  shopAdmins: any;

  constructor(
    private loginService: LoginService,
    private shopsService: ShopsDataService,
    private usersService: UsersDataService,
    private router: Router,
    public snackBar: MatSnackBar,
    public api: ShopService) { }

  ngOnInit() {
    this.shopAdmins = this.usersService.shopAdmin;
    this.getShops();
  }

  redirect() {
    this.router.navigate(['/edit-shop']);
  }

  getShops(){
    this.shopsService.getAllShops()
      .subscribe((shops) => {
        if (shops != null){
          this.shops = shops;
          // console.log("this.shops ", this.shops);
        } else {
          console.log("no shops found");
        }
      })
  }

  deleteShop(shopId) {
    var formData = new FormData;
    formData.append("shopId", shopId);
    
    this.api.delete(formData)
      .subscribe((response) => {
        if (response == null) {
          this.refreshShops();
        }
      })
  }

  refreshShops(){
    this.shopsService.refreshAllShops()
    .subscribe((shops) => {
      if (shops != null){
        this.shops = shops;
        // console.log("this.shops ", this.shops);
      } else {
        console.log("no shops found");
      }
    })
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


  // unclaim(shopId, userId){
  //   console.log(shopId, userId);

  //   for (let i = 0; i < this.shopAdmins.length; i++) {
  //     const shopAdmin = this.shopAdmins[i];
  //     if (shopAdmin.userId == userId) {
  //       this.shopAdmins.splice([i],1);
  //     }
  //   }
  //   console.log(this.shopAdmins);
  // }
}
