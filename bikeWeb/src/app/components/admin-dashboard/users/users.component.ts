import { Component, OnInit } from '@angular/core';
import { UsersDataService } from 'src/app/services/usersData/users-data.service';
import { ShopsDataService } from 'src/app/services/shopsData/shops-data.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: any;
  shops: any;

  constructor(
    private usersService: UsersDataService,
    private shopsService: ShopsDataService,
    private router: Router,
    public snackBar: MatSnackBar,) { }

  ngOnInit() {
    this.users = this.usersService.usersData;
    // this.shops = this.shopsService.bikeShopData; 
  }

  deleteUser(mail){
    // console.log("mail ", mail);
    this.users = this.usersService.usersData;
    for (let i = 0; i < this.users.length; i++) {
      const user = this.users[i];
      if (user.userMail == mail){
        console.log('match');
        this.users.splice([i],1);
      }
      
    }
    // console.log("this.users ", this.users);
  }

  redirectToEdit(shopId){
    this.router.navigate(['/edit-shop']);
  }

//   deleteShop(shopId) {
//  console.log("shopId ", shopId);
//     for (let i = 0; i < this.shops.length; i++) {
//       const shop = this.shops[i];
//       if (shop.shopId == shopId){
//         console.log('match');
//         this.shops.splice([i],1);
//         this.openSnackBar('Shop deleted', '');
//       }
      
//     }
 // }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
