import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BikeShopDetailsComponent } from './bike-shop-details.component';

describe('BikeShopDetailsComponent', () => {
  let component: BikeShopDetailsComponent;
  let fixture: ComponentFixture<BikeShopDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BikeShopDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BikeShopDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
