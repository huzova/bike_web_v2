import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { ShopService } from 'src/app/services/api/shop/shop.service';
import { ActivatedRoute, Router } from '@angular/router';

/* ************************************************* */

@Component({
  selector: 'app-bike-shop-details',
  templateUrl: './bike-shop-details.component.html',
  styleUrls: ['./bike-shop-details.component.scss']
})

/* ************************************************* */

export class BikeShopDetailsComponent implements OnInit {
  // properties
  createReviewForm: FormGroup;
  claimShopResponse: String;
  avg: number;
  shopReviewAvg = 3;
  errReviewMsg : boolean = false;

  shop : any;
  userId : any;
  shopId : any;
  openingHours : any; 
  userReviews : any ;
  canReview : boolean = true;

  starRatings = [
    {value: '1', viewValue: '1 *'},
    {value: '2', viewValue: '2 **'},
    {value: '3', viewValue: '3 ***'},
    {value: '4', viewValue: '4 ****'},
    {value: '5', viewValue: '5 *****'}
  ];
  
  /* ************************************************* */

  constructor( 
    private fb: FormBuilder,
    private loginService: LoginService,
    private api: ShopService, 
    private route: ActivatedRoute,
    private router: Router ) 
    {
      // validate inputs
      this.createReviewForm = this.fb.group({
        review: ['', Validators.required],
        stars:['', Validators.required]
      })
  }

  /* ************************************************* */

  ngOnInit() { 
    this.shopId = this.route.snapshot.params.shopId;
    this.getShopData();
  }

  /* ************************************************* */

  claimShop(){
    this.claimShopResponse = '';

    if(this.loginService.isLoggedIn){
      let formData = new FormData();
      formData.append('shopId', this.shopId);
      formData.append('securityToken', this.shop.securityToken);
      // console.log("formData ", formData);

      this.api.claim(formData)
      .subscribe((response) => {
        if (response != null){
          console.log("response err");
        } else {
          console.log("response OK");
        }
      })

      this.claimShopResponse = 'Shop claimed! You will be contacted to verify your autorization :)';

    } else {
      this.claimShopResponse = 'Please login first :)';
    }
  }

  /* ************************************************* */

  onSubmit(createReviewForm){

    let formData = new FormData();
    formData.append('shopId', this.shopId);
    formData.append('review', createReviewForm.value.review);
    formData.append('rating', createReviewForm.value.stars);
    formData.append('securityToken', this.shop.securityToken);

    this.api.saveShopReview(formData)
    .subscribe((response) => {
      if(response == "Review already written"){
        this.errReviewMsg = true;
      }
    });

    this.getReviews(formData);

    createReviewForm.reset();
    this.canReview = false;
  }

  /* ************************************************* */

  getShopData(){
    let formData = new FormData();
    formData.append('shopId', this.shopId);
    
    this.api.getShopBasicCategoriesById(formData)
    .subscribe((response) => {
      if (response != null){
        this.shop = response;
        // console.log("Shop data ", response);
      } else {
        console.log("Shop not found ");
        this.router.navigate(['page-not-found'])
      }
    });

    this.getOpeningHoursById(formData);
    this.getReviews(formData);
  }

  convert(img){
    var newImg = "http://localhost:8888/bike_web_v2.1/" + img;
    return newImg
  }

  /* ************************************************* */

  getOpeningHoursById(formData){
    this.api.getOpenningHoursByShopId(formData)
    .subscribe((openingHours) => {
      if (openingHours != null){
        this.openingHours = openingHours;
        // console.log("openingHours ", openingHours);
      } else {
        console.log("Opening Hours not found ");
        this.router.navigate(['page-not-found'])
      }
    });
  }

  /* ************************************************* */
  
  getReviews(formData){
    this.api.getReviewsByShopId(formData)
    .subscribe((userReviews) => {
      if (userReviews != null){
        this.userReviews = userReviews[0];
        this.shopReviewAvg = userReviews[1];
      } else {
        // console.log("Opening Hours not found ");
        this.router.navigate(['page-not-found'])
      }
    });
  }
 
  /* ************************************************* */



}
