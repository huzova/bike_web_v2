import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBikeShopComponent } from './create-bike-shop.component';

describe('CreateBikeShopComponent', () => {
  let component: CreateBikeShopComponent;
  let fixture: ComponentFixture<CreateBikeShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBikeShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBikeShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
