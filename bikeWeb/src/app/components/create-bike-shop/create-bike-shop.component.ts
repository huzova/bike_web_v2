import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ShopService } from 'src/app/services/api/shop/shop.service';

/* ************************************************* */

@Component({
  selector: 'app-create-bike-shop',
  templateUrl: './create-bike-shop.component.html',
  styleUrls: ['./create-bike-shop.component.scss']
})

/* ************************************************* */

export class CreateBikeShopComponent implements OnInit {
  // properties
  firstFormGroup : FormGroup;
  secondFormGroup : FormGroup;
  thirdFormGroup : FormGroup;
  fourthFormGroup : FormGroup;

  longitude : any;
  latitude : any;
  formattedAddress : any;

  pricePoints = [
    {value: '$', viewValue: '$'},
    {value: '$$', viewValue: '$$'},
    {value: '$$$', viewValue: '$$$'}
  ];

  times = [
    {value: 'closed', viewValue: 'closed'},
    {value: '0', viewValue: '0:00'},
    {value: '1', viewValue: '1:00'},
    {value: '2', viewValue: '2:00'},
    {value: '3', viewValue: '3:00'},
    {value: '4', viewValue: '4:00'},
    {value: '5', viewValue: '5:00'},
    {value: '6', viewValue: '6:00'},
    {value: '7', viewValue: '7:00'},
    {value: '8', viewValue: '8:00'},
    {value: '9', viewValue: '9:00'},
    {value: '10', viewValue: '10:00'},
    {value: '11', viewValue: '11:00'},
    {value: '12', viewValue: '12:00'},
    {value: '13', viewValue: '13:00'},
    {value: '14', viewValue: '14:00'},
    {value: '15', viewValue: '15:00'},
    {value: '16', viewValue: '16:00'},
    {value: '17', viewValue: '17:00'},
    {value: '18', viewValue: '18:00'},
    {value: '19', viewValue: '19:00'},
    {value: '20', viewValue: '20:00'},
    {value: '21', viewValue: '21:00'},
    {value: '22', viewValue: '22:00'},
    {value: '23', viewValue: '23:00'},
    {value: '24', viewValue: '24:00'}
  ];

  openingTime = '8';
  closingTime = '17';

  days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

  errMsg : boolean = false;

  /* ************************************************* */

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private loginService: LoginService,
    private api: ShopService)
    {
      // validate inputs firstFormGroup
      this.firstFormGroup = this.fb.group({
        bikeShopName: ['', Validators.required],
        bikeShopAddress: ['', Validators.required],
        bikeShopPhone: ['', Validators.required],
        bikeShopPricePoint: ['', Validators.required]
      });
      // validate inputs secondFormGroup
      this.secondFormGroup = this.fb.group({
        isNewBikeSeller: [''],
        is2ndHandBikeSeller: [''],
        isBikeRental: [''],
        isBikeRepair: [''],
        isAccessoriesSeller: ['']
      });
      // validate inputs thirdFormGroup
      this.thirdFormGroup = this.fb.group({
        MondayOpen: [''],
        MondayClose: [''],
        TuesdayOpen: [''],
        TuesdayClose: [''],
        WednesdayOpen: [''],
        WednesdayClose: [''],
        ThursdayOpen: [''],
        ThursdayClose: [''],
        FridayOpen: [''],
        FridayClose: [''],
        SaturdayOpen: [''],
        SaturdayClose: [''],
        SundayOpen: [''],
        SundayClose: ['']
      });
      // validate inputs fourthFormGroup
      // this.fourthFormGroup = this.fb.group({
      //   shopImage: [''],
      // });
    }

  /* ************************************************* */

  ngOnInit() {
  }
  
  /* ************************************************* */
  
  onSubmitForm() {
    if (this.longitude == undefined || this.latitude == undefined){
      this.errMsg = true;
    } else {
      this.errMsg = false;

      let formData = new FormData();
      formData.append('shopName', this.firstFormGroup.value.bikeShopName);
      formData.append('shopAddress', this.formattedAddress);
      formData.append('shopPhone', this.firstFormGroup.value.bikeShopPhone);
      formData.append('pricePoint', this.firstFormGroup.value.bikeShopPricePoint);
      formData.append('longitude', this.longitude);
      formData.append('latitude', this.latitude);

      formData.append('isNewBikeSeller', this.secondFormGroup.value.isNewBikeSeller);
      formData.append('is2ndHandBikeSeller', this.secondFormGroup.value.is2ndHandBikeSeller);
      formData.append('isBikeRental', this.secondFormGroup.value.isBikeRental);
      formData.append('isBikeRepair', this.secondFormGroup.value.isBikeRepair);
      formData.append('isAccessoriesSeller', this.secondFormGroup.value.isAccessoriesSeller);
      formData.append('MondayOpen', this.thirdFormGroup.value.MondayOpen);
      formData.append('MondayClose', this.thirdFormGroup.value.MondayClose);
      formData.append('TuesdayOpen', this.thirdFormGroup.value.TuesdayOpen);
      formData.append('TuesdayClose', this.thirdFormGroup.value.TuesdayClose);
      formData.append('WednesdayOpen', this.thirdFormGroup.value.WednesdayOpen);
      formData.append('WednesdayClose', this.thirdFormGroup.value.WednesdayClose);
      formData.append('ThursdayOpen', this.thirdFormGroup.value.ThursdayOpen);
      formData.append('ThursdayClose', this.thirdFormGroup.value.ThursdayClose);
      formData.append('FridayOpen', this.thirdFormGroup.value.FridayOpen);
      formData.append('FridayClose', this.thirdFormGroup.value.FridayClose);
      formData.append('SaturdayOpen', this.thirdFormGroup.value.SaturdayOpen);
      formData.append('SaturdayClose', this.thirdFormGroup.value.SaturdayClose);
      formData.append('SundayOpen', this.thirdFormGroup.value.SundayOpen);
      formData.append('SundayClose', this.thirdFormGroup.value.SundayClose);
      // formData.append('shopImage', this.fourthFormGroup.controls['shopImage'].value);
      
      // send data
      this.api.create(formData).subscribe((response) => {
        if (response != null){
          this.errMsg = true;
        }
      });
    }
  }

  /* ************************************************* */

  processSelectedAddress(event : any) {
    this.longitude = event.geometry.location.lng();
    this.latitude = event.geometry.location.lat();
    this.formattedAddress = CreateBikeShopComponent.getFormattedAddress(event)['formatted_address'];
    console.log("Found lon:" +  this.longitude + ", lat: " + this.latitude + " with address " + JSON.stringify(this.formattedAddress));
  }

  /* ************************************************* */

  static getFormattedAddress(place) {
      let location_obj = {};
      for (let i in place.address_components) {
          let item = place.address_components[i];

          location_obj['formatted_address'] = place.formatted_address;
          if (item['types'].indexOf("locality") > -1) {
              location_obj['locality'] = item['long_name']
          } else if (item['types'].indexOf("administrative_area_level_1") > -1) {
              location_obj['admin_area_l1'] = item['short_name']
          } else if (item['types'].indexOf("street_number") > -1) {
              location_obj['street_number'] = item['short_name']
          } else if (item['types'].indexOf("route") > -1) {
              location_obj['route'] = item['long_name']
          } else if (item['types'].indexOf("country") > -1) {
              location_obj['country'] = item['long_name']
          } else if (item['types'].indexOf("postal_code") > -1) {
              location_obj['postal_code'] = item['short_name']
          }
      }
      return location_obj;
  }

  /* ************************************************* */
}
