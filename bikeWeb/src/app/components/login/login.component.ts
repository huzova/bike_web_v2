import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from 'src/app/services/login.service';
import {UsersDataService} from 'src/app/services/usersData/users-data.service';
import {ApiService} from "../../services/api/api.service";
import { LocalStorageService } from 'src/app/services/localStorage/local-storage.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
    // proprties
    loginForm : FormGroup;
    loginErr : boolean = false;
    isTimeout : boolean = false;

    constructor(
        private fb: FormBuilder,
        private loginService: LoginService,
        private usersData: UsersDataService,
        private api: ApiService,
        private ls: LocalStorageService) {

        // validate inputs
        this.loginForm = this.fb.group({
            userMail: ['', Validators.required],
            userPassword: ['', Validators.required]
        });
    }

    ngOnInit() {
    }

    onSubmit() {
        let formData = new FormData();
        formData.append('mail', this.loginForm.controls['userMail'].value);
        formData.append('password', this.loginForm.controls['userPassword'].value);

        this.api.login(formData).subscribe((user) => {
            if (user.data) {
                // console.log("user ", user);
                this.loginErr = false;
                this.isTimeout = false;
                this.loginService.changeRoutes(user.data);
            } else {
                if(user.timeOut){
                    // console.log("user ", user);
                    this.loginErr = false;
                    this.isTimeout = true;
                } else {
                    // console.log("user ", user);
                    this.loginErr = true;
                    this.isTimeout = false;
                }
            }
        });
    }
}
