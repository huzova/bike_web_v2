/// <reference types="@types/googlemaps" />

/****************************************/

import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ShopsDataService } from 'src/app/services/shopsData/shops-data.service';
import { ShopService } from 'src/app/services/api/shop/shop.service';
import { Inject }  from '@angular/core';
import { DOCUMENT } from '@angular/common';
import {FilterShopService} from "../../services/shopFilter/filter-shop.service";
import { observable } from 'rxjs';
// import { } from 'googlemaps';

/****************************************/

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})

/****************************************/

export class MapComponent implements OnInit {

  // properties
  searchForm : FormGroup;
  userLat : any;
  userLng : any;
  shops : any = [];

  markers : google.maps.Marker[] = [];

  searchOptionText : string;
  searchOptionSellsNew : boolean;
  searchOptionSells2ndHand : boolean;
  searchOptionSellsAccessories : boolean;
  searchOptionRental : boolean;
  searchOptionOpenNow : boolean;
  searchOption$ : boolean;
  searchOption$$ : boolean;
  searchOption$$$ : boolean;

  private filterShopService = new FilterShopService();

  /****************************************/

  constructor(
    private fb: FormBuilder, 
    private shopData: ShopsDataService,
    private api: ShopService,)
    //@Inject(DOCUMENT) document) 
    {   
      // this.search = document.getElementById('autocomplete');
      // console.log("this.search ", this.search);

      // validate input
      this.searchForm = this.fb.group({
        search: ['', Validators.required]
      })
    }
  
  /****************************************/

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;
  
  /****************************************/

  ngOnInit() {
    var shops = this.shopData.shopsNearby;
    if (shops == undefined){
      this.getUserLocation();
      // console.log("this.shops ", this.shops);
    } else {
      this.shops = shops;
      // console.log("this.shops ", this.shops);
      this.getUserLocation();
    }
  }

  /****************************************/

  // submit frm on enter
  // keyDownFunction(event) {
  //   if(event.keyCode == 13) {
  //     // console.log(this.searchForm.value);
  //   }
  // }

  /****************************************/

  // get user loacation
  getUserLocation() {
    console.log('getting user location', navigator.geolocation);
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.userLat = position.coords.latitude;
        this.userLng = position.coords.longitude;
        
        // console.log("getUserLocation this.userLng ", this.userLng);
      
        if (this.userLat != undefined || this.userLng != undefined){
          // console.log("this.shops ", this.shops);
          if (this.shops.length > -1){
            this.getShopsNearby();
          } else {
            this.configMapAndMarkers();
          }

        } else {
          this.useDefautPosition();
        }

      }, (error) =>  {
          console.log("error");
          alert("Please allow geolocation services. Error: " + error.message);
      });

    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }
  
  /****************************************/
 
  useDefautPosition() {
    setTimeout(() => { 
      if(this.userLat == undefined || this.userLng == undefined) {
        this.userLat = 55.679470;
        this.userLng = 12.585267;
        // console.log("setTimeout this.userLng ", this.userLng);
        if (this.shops.length > -1){
          this.getShopsNearby();
        } else {
          this.configMapAndMarkers();
        }
      }
    }, 1000);
  }

  /****************************************/

  configMapAndMarkers(){
    // map confi
    let location = new google.maps.LatLng(this.userLat, this.userLng);
    // console.log("this.userLat, this.userLng ", this.userLat, this.userLng);
    var mapProp = {
      center: location,
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    // for each bikeshop show marker
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);

    for (let i = 0; i < this.shops.length; i++) {
      const shop = this.shops[i];
      if (shop.latitude){
        let lat = Number(shop.latitude);
        let lng = Number(shop.longitude);
        this.createMarker(lat, lng, shop.shopName);
      }
    }
  }

  /****************************************/
  
  // get all Shops
  getShopsNearby(){
    // console.log("getShopsNearby this.userLat ", this.userLat);
    this.shopData.getShopsNearby(this.userLat, this.userLng)
      .subscribe((shops) => {
        if (shops != null){
          this.shops = shops;
          // console.log("this.shops ", this.shops);
          this.configMapAndMarkers();
        } else {
          console.log("no shops found");
        }
      })
  }
    
  /****************************************/

  // create Marker
  createMarker(shopLat, shopLng, shopName){
    let marker = new google.maps.Marker({
      position: {lat: shopLat, lng: shopLng},
      map: this.map,
      title: shopName
    });
    this.markers.push(marker);
  }

  searchOptionsChanged() {
    const filteredShops = this.filterShopService.filter(this.shops, this.searchOptionText, this.searchOptionSellsNew,
        this.searchOptionSells2ndHand,this.searchOptionSellsAccessories,
        this.searchOptionRental, this.searchOptionOpenNow, this.searchOption$, this.searchOption$$, this.searchOption$$$);

    this.markers.forEach(marker => marker.setMap(null));
    this.markers = [];

    for (let i = 0; i < filteredShops.length; i++) {
        const shop = filteredShops[i];
        if (shop.latitude) {
            let lat = Number(shop.latitude);
            let lng = Number(shop.longitude);
            this.createMarker(lat, lng, shop.shopName);
        }
    }
  }
}
