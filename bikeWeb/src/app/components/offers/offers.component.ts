import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'src/app/services/api/message/message.service';
import { MatSnackBar } from '@angular/material';

/* ************************************************* */

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})

/* ************************************************* */

export class OffersComponent implements OnInit {
  
  // properties
  offerForm : FormGroup;
  // loggedInUser = {"id":14, "userMail": "b@b.com", "userName": "bbb"};
  offers : any;

  /* ************************************************* */

  constructor(
    private loginService: LoginService,
    private fb: FormBuilder,
    private api: MessageService,
    public snackBar: MatSnackBar ) 
    { 
      // validate inputs
      this.offerForm = this.fb.group({
        offerText:['', Validators.required],
        // userId:[this.loggedInUser.id], 
      });
  }

  /* ************************************************* */
  
  ngOnInit() {
    if (this.loginService.isLoggedIn){
      this.api.getMsgs()
        .subscribe((messages) => {
          if (messages != null) {
            // console.log("offers ", messages);
            this.offers = messages;
          } else {
            console.log("no messages");
          }
        })
    }
  }

  /* ************************************************* */

  onSubmit() {
    let formData = new FormData();
    formData.append('message', this.offerForm.controls['offerText'].value);

    this.api.createMsg(formData)
      .subscribe((error) => {
        if (error == null) {
          console.log("Message created");
        } else {
          console.log("Error");
        }
      })
      
      this.openSnackBar('Offer created :)', '');
  }

  /* ************************************************* */
  
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  /* ************************************************* */

}
