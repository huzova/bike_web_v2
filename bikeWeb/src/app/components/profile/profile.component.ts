import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { TimeService } from 'src/app/services/timeData/time.service';
import { SelectDataService } from 'src/app/services/selectData/select-data.service';
import { ApiService } from 'src/app/services/api/api.service';
import { LocalStorageService } from 'src/app/services/localStorage/local-storage.service';
import { ShopService } from 'src/app/services/api/shop/shop.service';

/* ************************************************* */

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

/* ************************************************* */

export class ProfileComponent implements OnInit {
  
  // properties
  updateProfileForm: FormGroup;
  // updateShopFrm: FormGroup;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;

  times: any; // [];
  days: any; // [];
  pricePoints: any; // [];

  userProfile: any;
  
  shops : any;
  
  /* ************************************************* */

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private loginService: LoginService,
    public snackBar: MatSnackBar,
    private timeService: TimeService,
    private selectService: SelectDataService,
    private api: ApiService,
    private apiShop : ShopService,
    private ls: LocalStorageService,) {
      
      // validate user frm
      this.updateProfileForm = this.fb.group({
        userMail: ['', Validators.required],
        userName: ['', Validators.required],
        userPassword: [''],
        newUserPassword: [''],
        isBikeAdmin: [''],
        userImg: [''],
        dbPassword:['']
      });
    }

  /* ************************************************* */
 
  ngOnInit() {
    // get data from services
    this.times = this.timeService.timeData;
    this.days = this.timeService.dayData;
    this.pricePoints = this.selectService.pricePointData;
    
    // get user
    this.api.getUser().subscribe((user) => {
      if (user != null) {
        this.userProfile = user;
        console.log("this.userProfile ", this.userProfile);
        this.constructFrm(this.userProfile);
      } else {
        console.log("can't find user");
      }
    });

    this.getShops();
  }

  /* ************************************************* */

  updateUser(){
    let formData = new FormData();
    formData.append("userMail", this.updateProfileForm.value.userMail);
    formData.append("userName", this.updateProfileForm.value.userName);
    formData.append("userPassword", this.updateProfileForm.value.userPassword);
    formData.append("newUserPassword", this.updateProfileForm.value.newUserPassword);
    console.log("formData ", formData);

    this.api.updateUser(formData)
    .subscribe((response) => {
      this.openSnackBar('Hurray! Profile updated :)', '');
    });
  }

  /* ************************************************* */
  
  // construct forms
  constructFrm(userProfile) {
    console.log("userProfile ", userProfile);
    this.updateProfileForm = this.fb.group({
      userMail: [userProfile == null ? '' : userProfile.userMail, Validators.required],
      userName: [userProfile == null ? '' : userProfile.userName, Validators.required],
      userPassword: [''],
      newUserPassword: [''],
      isBikeAdmin: [userProfile == null ? '' : userProfile.isBikeAdmin],
      userImg: [''],
      dbPassword:[userProfile == null ? '' : userProfile.userPassword]
    })
  }

  /* ************************************************* */

  deleteUser(){
    console.log("deleting user");
    this.api.deleteUser()
    .subscribe((response) => {
      console.log("response", response);
    });
    this.loginService.logout();
  }

  /* ************************************************* */

  // toHide(message) {
  //   this.openSnackBar(message, '');
  //   this.shops.splice(0,1);
  // }

  /* ************************************************* */

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  getShops(){
    this.apiShop.getShopsByUserId()
    .subscribe((response) => {
      console.log("response", response);
      if(response == null){
        this.shops = [];
      } else {
        this.shops = response;
      }
    });
  }

}
