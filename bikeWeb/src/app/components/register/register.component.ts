import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ApiService} from "../../services/api/api.service";
import {LoginService} from "../../services/login.service";
import { AuthService, FacebookLoginProvider } from 'angular-6-social-login-v2';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    registerForm : FormGroup;
    registerErr : boolean = false;

    constructor(
        private fb: FormBuilder,
        private loginService: LoginService,
        private api: ApiService,
        private socialAuthService: AuthService ) 
        {
            // validate inputs
            this.registerForm = this.fb.group({
                userMail: ['', Validators.required],
                userName: ['', Validators.required],
                userPassword: ['', Validators.required],
                isBikeAdmin: [''],
                userImg: ['']
            });
    }

    ngOnInit() {}

    public socialSignIn(socialPlatform : string) {
        let socialPlatformProvider;
        if(socialPlatform == "facebook"){
          socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
        }

        this.socialAuthService.signIn(socialPlatformProvider).then(
            (userData) => {
                // console.log(socialPlatform + " sign in data : " , userData);
                this.registerForm.controls['userMail'].setValue(userData.email);
                this.registerForm.controls['userName'].setValue(userData.name);
            }
        );
    }

    onSubmit() {
        this.registerUser();
    }

    registerUser(){
        // console.log(this.registerForm.value);

        let formData = new FormData();
        formData.append('userMail', this.registerForm.controls['userMail'].value);
        formData.append('userName', this.registerForm.controls['userName'].value);
        formData.append('userPassword', this.registerForm.controls['userPassword'].value);
        formData.append('isBikeAdmin', this.registerForm.controls['isBikeAdmin'].value);
        formData.append('userImg', this.registerForm.controls['userImg'].value);


        this.api.signUp(formData)
            .subscribe((user) => {
                if (user != null) {
                    console.log(user);
                    this.loginService.changeRoutes(user);
                } else {
                    this.registerErr = true;
                    // console.log("can't register");
                }
            });
    }

}
