import {Directive, ElementRef, EventEmitter, OnInit, Output} from '@angular/core';
/// <reference types="@types/googlemaps" />

@Directive({
    selector: '[appGoogleAddressAutocomplete]'
})
export class GoogleAddressAutocompleteDirective implements OnInit {
    @Output() onSelect: EventEmitter<any> = new EventEmitter();
    private readonly element: HTMLInputElement;

    constructor(elRef: ElementRef) {
        //elRef will get a reference to the element where
        //the directive is placed
        this.element = elRef.nativeElement;
    }

    ngOnInit() {
        const autocomplete = new google.maps.places.Autocomplete(this.element);
        //Event listener to monitor place changes in the input
        google.maps.event.addListener(autocomplete, 'place_changed', () => {
            //Emit the new address object for the updated place
            this.onSelect.emit(autocomplete.getPlace());
        });
    }
}