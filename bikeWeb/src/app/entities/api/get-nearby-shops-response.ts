import {Shop} from "../shop";

export class GetNearbyShopsResponse {
    ok: boolean;
    status: string;
    data: Shop[];
}
