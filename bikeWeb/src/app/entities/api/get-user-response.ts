import {RegisterResponse} from "./register-response";

export interface GetUserResponse extends RegisterResponse {}