import {User} from "../user";

export interface LoginResponse {
    ok: boolean;
    status: string;
    data: User;
    timeOut : boolean;
}
