import {User} from "../user";

export interface RegisterResponse {
    ok: boolean;
    status: string;
    data: User;
    securityToken: string;
    message: string;
}
