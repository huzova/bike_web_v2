export interface ReviewResponse {
    ok: boolean;
    status: string;
    data: any;
    rating: any;
    securityToken: string;
}
