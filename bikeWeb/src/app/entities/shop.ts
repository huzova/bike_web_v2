export class Shop {

    public shopId;
    public shopName;
    public shopEmail;
    public shopPhone;
    public shopAddress;
    public shopWebsite;
    public pricePoint;
    public isApproved;

    public latitude;
    public longitude;

    public sellsNewBikes : boolean;
    public sells2ndHandBikes : boolean;
    public isRental : boolean;
    public isBikeRepair : boolean;
    public sellsAccessories : boolean;

}
