export class User {
    constructor() {

    }

    id ?: String;
    userMail: String;
    userName: String;
    userPassword: String;
    isBikeAdmin ?: boolean;
    isAdmin ?: boolean;
    userImg ?: String;
}