import {Pipe, PipeTransform} from '@angular/core';
import {Shop} from "../entities/shop";
import {FilterShopService} from "../services/shopFilter/filter-shop.service";

@Pipe({
    name: 'filterShops'
})
export class FilterShopsPipe implements PipeTransform {
    private filterShopService = new FilterShopService();
    constructor() {
        
    }

    transform(shops: Shop[],
              searchText: string,
              sellsNew: boolean,
              sells2ndHand: boolean,
              sellsAccessories: boolean,
              isRental: boolean,
              isOpenNow: boolean,
              is$: boolean,
              is$$: boolean,
              is$$$: boolean): Shop[] {

        return this.filterShopService.filter(shops, searchText, sellsNew, sells2ndHand, sellsAccessories, isRental, isOpenNow, is$, is$$, is$$$);
    }
}
