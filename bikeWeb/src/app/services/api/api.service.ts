import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {User} from "../../entities/user";
import {HttpClient} from "@angular/common/http";
import {LoginComponent} from "../../components/login/login.component";
import {LoginResponse} from "../../entities/api/login-response";
import {RegisterResponse} from "../../entities/api/register-response";
import { GetUserResponse } from 'src/app/entities/api/get-user-response';
import {environment} from "../../../environments/environment";

/* ************************************************* */

@Injectable({
    providedIn: 'root'
})

/* ************************************************* */

export class ApiService {
    // properties
    private API_BASE =  environment.apiRoot + '/api';

    constructor( private http: HttpClient ) { }
    
    /* ************************************************* */
    
    // Login
    public login(formData : FormData): Observable<any> {
        return this.http
            .post<LoginResponse>(this.API_BASE + '/auth/login.php',
                formData,
                {withCredentials : true})
            .map(response => {
                return response;
            })
            .catch(this.handleError);
    }

    /* ************************************************* */
    
    // Sign Up
    public signUp(formData : FormData): Observable<any> {
        return this.http
            .post<RegisterResponse>(this.API_BASE + '/user/create.php',
                formData, {withCredentials : true})
            .map(response => {
                console.log('Register server response', response);
                if (!response.ok) {
                    // is err
                    return null;
                }
                else
                    return response.data;
            })
            .catch(this.handleError);
    }

    /* ************************************************* */
    
    // Get user
    public getUser(): Observable<any>{
        //userId.append('token', this.ls.getFromLocal("token"));
        return this.http
        .post<GetUserResponse>(this.API_BASE + '/user/get.php', '' ,
            {withCredentials : true})
        .map(response => {
            console.log('Get user server response', response);
            if (response.data == null) {
                // is err
                return null;
            }
            else
                return response.data;
        })
        .catch(this.handleError);
    }

    /* ************************************************* */

    // Get user and see if response is correct or not
    public checkUserLoggedIn(): Observable<any>{
        return this.http
        .post<GetUserResponse>(this.API_BASE + '/user/get.php', '' ,
            {withCredentials : true})
        .catch(this.handleError);
    }

    public logout(): Observable<any>{
        return this.http.post<GetUserResponse>(this.API_BASE + '/auth/logout.php', '',
            {withCredentials : true})
        .catch(this.handleError);
    }

    /* ************************************************* */

    // Update user
    public updateUser(formData : FormData): Observable<any>{
        return this.http
        .post<GetUserResponse>(this.API_BASE + '/user/updateProfile.php', formData ,
            {withCredentials : true})
        .map(response => {
            console.log('Update user server response', response);
            if (response == null) {
                return null;
            }
            else
                // is err
                return response;
        })
        .catch(this.handleError);
    }

    /* ************************************************* */

    // Delete user
    public deleteUser(): Observable<any>{
        return this.http
        .post<GetUserResponse>(this.API_BASE + '/user/delete.php', '' ,
            {withCredentials : true})
        .map(response => {
            console.log('delete user server response', response);
            if (response == null) {
                return null;
            }
            else
                // is err
                return response;
        })
        .catch(this.handleError);
    }

    /* ************************************************* */
    
    // handle Error
    private handleError(error: Response | any) {
        console.error('ApiService::handleError', error);
        return Observable.throw(error);
    }
}
