import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { RegisterResponse } from 'src/app/entities/api/register-response';
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  // properties
  private API_BASE = environment.apiRoot + '/api';

  constructor( private http: HttpClient) { }

  /* ************************************************* */

  // create mgs 
  public createMsg(formData : FormData): Observable<any> {
    return this.http
        .post<RegisterResponse>(this.API_BASE + '/message/create.php',
          formData, {withCredentials : true})
        .map(response => {
            console.log('Create server response', response);
            if (!response.ok) {
                // is err
                return response.status;
            } else
                return null;
        })
        .catch(this.handleError);
  }
  /* ************************************************* */

  // get all msgs
  public getMsgs(): Observable<any> {
    return this.http
        .post<RegisterResponse>(this.API_BASE + '/message/getAll.php',
            '', {withCredentials : true})
        .map(response => {
            console.log('Get server response', response);
            if (!response.data) {
                // is err
                return null;
            }
            else
                return response.data;
        })
        .catch(this.handleError);
  }

  /* ************************************************* */

  // handle Error
  private handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

}
