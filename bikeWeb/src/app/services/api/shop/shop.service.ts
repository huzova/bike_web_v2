import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { RegisterResponse } from 'src/app/entities/api/register-response';
import { ReviewResponse } from 'src/app/entities/api/review-response';
import {environment} from "../../../../environments/environment";
import {GetNearbyShopsResponse} from "../../../entities/api/get-nearby-shops-response";

/****************************************/

@Injectable({
  providedIn: 'root'
})

/****************************************/

export class ShopService {
    // properties
    private API_BASE = environment.apiRoot + '/api/shop';

    constructor( private http: HttpClient ) { }

    /****************************************/
    /*              GET                     */
    /****************************************/
  
    // get All Shops Basic Categories
    public getAllShopsBasicCategories(): Observable<any> {
        return this.http
        .post<RegisterResponse>(this.API_BASE + '/getAllShopsBasicCategories.php',
            '',
            {withCredentials : true})
        .map(response => {
            // console.log(response);
            if (!response.ok) // is err
                return null;
            else
                return response.data;
        })
        .catch(this.handleError);
    }
    
    /****************************************/
    // get All Shops Nearby
    public getAllShopsNearby(lat, lng, radius): Observable<any> {
        return this.http
        .get<GetNearbyShopsResponse>(this.API_BASE + '/getNearby.php?lon='+ lng +'&lat='+ lat +'&radius='+ radius,
            {withCredentials : true})
        .map(response => {
            if (!response.ok) // is err
            return null;
        else
            return response.data;
        })
        .catch(this.handleError);
    }

    /****************************************/
  
    // get Shops To Approve
    public getShopsToApprove(): Observable<any> {
        return this.http
        .post<RegisterResponse>(this.API_BASE + '/getShopsToApprove.php',
            '',
            {withCredentials : true})
        .map(response => {
            // console.log(response);
            if (response.ok) // is err
                return response.data;
            else
            // console.log("response.data ", response.data);
                return response.data;
        })
        .catch(this.handleError);
    }

    /****************************************/

    // get Shop Basic and Categories By shopId
    public getShopBasicCategoriesById(formData : FormData): Observable<any> {
        return this.http
        .post<RegisterResponse>(this.API_BASE + '/getShopBasicCategoriesById.php',
            formData,
            {withCredentials : true})
        .map(response => {
            // console.log(response);
            if (!response.ok) // is err
                return null;
            else
                return response.data;
        })
        .catch(this.handleError);
    }

    /****************************************/

    // get opening hours by shop id 
    public getOpenningHoursByShopId(formData : FormData): Observable<any> {
        return this.http
        .post<RegisterResponse>(this.API_BASE + '/getOpenningHoursByShopId.php',
            formData,
            {withCredentials : true})
        .map(response => {
            // console.log(response);
            if (!response.ok) // is err
                return null;
            else
                return response.data;
        })
        .catch(this.handleError);
    }

    /****************************************/
    
    // get reviews and rating
    public getReviewsByShopId(formData : FormData): Observable<any> {
        return this.http
        .post<ReviewResponse>(this.API_BASE + '/getReviewsByShopId.php',
            formData,
            {withCredentials : true})
        .map(response => {
            // console.log(response);
            if (!response.ok) // is err
                return null;
            else
                var reviews =response.data;
                var rating = response.rating;
                var data = [reviews, rating];
                return data;
        })
        .catch(this.handleError);
    }

     /****************************************/

     // get reviews and rating
     public getNotApprovedShops(): Observable<any> {
        return this.http
        .post<RegisterResponse>(this.API_BASE + '/getNotApprovedShops.php',
            '',
            {withCredentials : true})
        .map(response => {
            // console.log(response);
            if (response == null) // is err
                return null;
            else
                return response;
        })
        .catch(this.handleError);
    }

    /****************************************/

     // get reviews and rating
     public getShopsByUserId(): Observable<any> {
        return this.http
        .post<RegisterResponse>(this.API_BASE + '/getShopsByUserId.php',
            '',
            {withCredentials : true})
        .map(response => {
            // console.log(response);
            if (response.ok == false) // is err
                return null;
            else
                return response.data;
        })
        .catch(this.handleError);
    }

    /****************************************/
    /*            CLAIM                     */
    /****************************************/

    // claim shop
    public claim(formData : FormData): Observable<any> {
        return this.http
        .post<RegisterResponse>(this.API_BASE + '/claim.php',
            formData,
            {withCredentials : true})
        .map(response => {
            // console.log(response);
            if (!response.ok) // is err
                return null;
            else
                return response.data;
        })
        .catch(this.handleError);
    }

    /****************************************/
    /*             SAVE                     */
    /****************************************/
    
    // save review
    public saveShopReview(formData : FormData): Observable<any> {
        return this.http
        .post<RegisterResponse>(this.API_BASE + '/saveShopReview.php',
            formData,
            {withCredentials : true})
        .map(response => {
            // console.log(response);
            if (!response.ok) // is err
                return response.message;
            else
                return response.data;
        })
        .catch(this.handleError);
    }

    /****************************************/
    
    // save shop 
    public create(formData : FormData): Observable<any> {
        return this.http
        .post<RegisterResponse>(this.API_BASE + '/create.php',
            formData, {withCredentials : true})
        .map(response => {
            if (!response.ok)
                return "is err";
            else
                return null;
        })
        .catch(this.handleError);
    }

    /****************************************/
    /*             DELETE                   */
    /****************************************/

    // delete shop 
    public delete(formData : FormData): Observable<any> {
        return this.http
        .post<RegisterResponse>(this.API_BASE + '/delete.php',
            formData, {withCredentials : true})
        .map(response => {
            // console.log("response ", response);
            if (!response.ok)
                return "is err";
            else
                return null;
        })
        .catch(this.handleError);
    }
    
    /****************************************/
    /*             UPDATE                   */
    /****************************************/

     // approve shop 
     public approve(formData : FormData): Observable<any> {
        return this.http
        .post<RegisterResponse>(this.API_BASE + '/approve.php',
            formData, {withCredentials : true})
        .map(response => {
            if (!response.ok)
                return "is err";
            else
                return null;
        })
        .catch(this.handleError);
    }

    /****************************************/
    /*              ERR                     */
    /****************************************/

    // handle Error
    private handleError(error: Response | any) {
        console.error('ShopService::handleError', error);
        return Observable.throw(error);
    }


}
