import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UsersDataService } from './usersData/users-data.service';
import { User } from '../entities/user';
import { ApiService } from './api/api.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn: boolean;
  isAdmin: boolean;
  isBikeAdmin: boolean;

  constructor(
    private router: Router,
    private api: ApiService) {
    // todo show loading
      // check if user is logged in 
      // if(this.hasLoginCookie()) {
      //   console.log("has Login Cookie");
        this.api.checkUserLoggedIn().subscribe((response) => {
          // console.log(response);
          if(response) {
            if(response.data) {
                // user is logged in
                this.setUserTypeByNumber(response.data);
            } else if (response.status) {
              // console.log(response.status);
                // if not - do nothing
                // console.log("can't find user");
            } else {
              // console.log(response);
            }
          } else {
            // console.log('no response');
          }
        });
      // } else console.log("does not have Login Cookie");
    }

  // find if phpsession cookie exists 
  // hasLoginCookie() : boolean {
  //   var cookies = document.cookie.split(";");
  //   for (var i = 0; i < cookies.length; i++) {
  //     var cookie = cookies[i];
  //     var eqPos = cookie.indexOf("=");
  //     var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
  //     if(name === 'PHPSESSID')
  //       return true;
  //   }
  //   return false;
  // }

  changeRoutes(user): any {
    this.setUserTypeByNumber(user);
    if(this.isAdmin){
      this.router.navigate(['admin-dashboard']);
    } else if (this.isBikeAdmin){
      this.router.navigate(['profile']);
    } else {
      this.router.navigate(['map']);
    }
  }

  // setUserType(user):any{
  //   this.isLoggedIn = true;
  //   if(Number(user.isAdmin) == 1){
  //     this.isAdmin = true;
  //   } else if (Number(user.isBikeAdmin) == 1 || user.isBikeAdmin){
  //     this.isBikeAdmin = true;
  //   } 
  // }

  setUserTypeByNumber(user):any{
    this.isLoggedIn = true;
    if(Number(user.isAdmin) == 1){
      this.isAdmin = true;
    } else if (Number(user.isBikeAdmin) == 1 ){
      this.isBikeAdmin = true;
    } 
  }

  logout(): void {
      this.api.logout().subscribe(response => {
          // console.log('logout response', response);
          this.isLoggedIn = false;
          this.isAdmin = false;
          this.isBikeAdmin = false;
          this.clearBrowserData();
          this.router.navigate(['map']);
      });
  }
  
  // remove all the data from local storage and cookies 
  // https://stackoverflow.com/questions/43145494/how-do-i-remove-cookies-when-the-browser-is-closed-using-angular2-cookies
  clearBrowserData() : void { 
    localStorage.clear();
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
  }
}
