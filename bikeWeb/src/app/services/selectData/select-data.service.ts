import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SelectDataService {
  
  pricePointData = [
    {value: '$', viewValue: '$'},
    {value: '$$', viewValue: '$$'},
    {value: '$$$', viewValue: '$$$'}
  ];

  constructor() { }
}
