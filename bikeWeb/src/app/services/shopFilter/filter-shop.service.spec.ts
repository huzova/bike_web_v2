import {TestBed} from '@angular/core/testing';

import {FilterShopService} from "./filter-shop.service";
import {Shop} from "../../entities/shop";

describe('ShopsDataService', () => {
    let service: FilterShopService;
    let testDataSet = createTestShops();
    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.get(FilterShopService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should find $ shops', () => {
        let shops = service.filter(createTestShops(), undefined, false, false,
            false, false, false, true, false, false);
        expect(shops.length).toBe(3);
    });

    it('should find shops containing "Holmb"', () => {
        let shops = service.filter(createTestShops(), "Holmb", false, false,
            false, false, false, false, false, false);
        expect(shops.length).toBe(1);
        expect(shops[0].shopId).toBe("OTg5");
    });

    it('should find all shops that sell new bikes and 2nd hand bikes', () => {
        let shops = service.filter(createTestShops(), '', true, true,
            false, false, false, false, false, false);
        expect(shops.length).toBe(1);
        expect(shops[0].shopId).toBe("OTk2");
    });
});

function createTestShops() : Shop[] {
    return [{
        "shopId": "OTg1",
        "shopName": "copenhagenbikeshop",
        "shopEmail": null,
        "shopPhone": "+45 35 34 33 45",
        "shopAddress": "Jagtvej 68, 2200 K&oslash;benhavn, Denmark",
        "shopWebsite": "http:\/\/www.copenhagenbikeshop.dk\/",
        "pricePoint": "$",
        "isApproved": null,
        "latitude": "55.6959",
        "longitude": "12.5509",
        "sellsNewBikes": null,
        "sells2ndHandBikes": null,
        "isRental": null,
        "isBikeRepair": null,
        "sellsAccessories": null
    }, {
        "shopId": "OTg2",
        "shopName": "N\u00f8rrebro' \u00e6&lt;h1&gt;alert();&lt;\/h1&gt;",
        "shopEmail": null,
        "shopPhone": "+45 33 12 78 79",
        "shopAddress": "Sankt Peders Str&aelig;de 30A, 1453 K&oslash;benhavn, Denmark",
        "shopWebsite": "http:\/\/sogreni.dk\/",
        "pricePoint": "$$",
        "isApproved": null,
        "latitude": "55.6789",
        "longitude": "12.5674",
        "sellsNewBikes": null,
        "sells2ndHandBikes": null,
        "isRental": null,
        "isBikeRepair": null,
        "sellsAccessories": null
    }, {
        "shopId": "OTg4",
        "shopName": "Ingerslevsgade 80, 1705 K&oslash;&lt;\/h1&gt;",
        "shopEmail": null,
        "shopPhone": "+45 26 70 02 29",
        "shopAddress": "Ingerslevsgade 80, 1705 København, Denmark",
        "shopWebsite": "http:\/\/baisikeli.dk",
        "pricePoint": "$$$",
        "isApproved": null,
        "latitude": "55.6656",
        "longitude": "12.5589",
        "sellsNewBikes": null,
        "sells2ndHandBikes": null,
        "isRental": null,
        "isBikeRepair": null,
        "sellsAccessories": null
    }, {
        "shopId": "OTg5",
        "shopName": "Holmbladsgade 38, 2300 ",
        "shopEmail": null,
        "shopPhone": "+45 32 20 20 57",
        "shopAddress": "Islands Brygge 15, 2300 K&oslash;benhavn, Denmark",
        "shopWebsite": "https:\/\/www.facebook.com\/Islandsbryggecykler\/",
        "pricePoint": "$$",
        "isApproved": null,
        "latitude": "55.6677",
        "longitude": "12.5782",
        "sellsNewBikes": null,
        "sells2ndHandBikes": null,
        "isRental": null,
        "isBikeRepair": null,
        "sellsAccessories": null
    }, {
        "shopId": "OTkw",
        "shopName": "Næstved 42s",
        "shopEmail": null,
        "shopPhone": "+45 32 96 34 28",
        "shopAddress": "Holmbladsgade 38, 2300 K&oslash;benhavn, Denmark",
        "shopWebsite": "http:\/\/www.cphbikeshop.dk\/",
        "pricePoint": "$$",
        "isApproved": null,
        "latitude": "55.6649",
        "longitude": "12.6063",
        "sellsNewBikes": null,
        "sells2ndHandBikes": true,
        "isRental": null,
        "isBikeRepair": null,
        "sellsAccessories": null
    }, {
        "shopId": "OTkx",
        "shopName": "Frederiksborggade 31, 1360 K&oslash;b",
        "shopEmail": null,
        "shopPhone": "+45 23 32 39 83",
        "shopAddress": "Fredensgade 15A, 2200 K&oslash;benhavn, Denmark",
        "shopWebsite": "http:\/\/www.lucascykler.dk\/",
        "pricePoint": "$",
        "isApproved": null,
        "latitude": "55.693",
        "longitude": "12.567",
        "sellsNewBikes": null,
        "sells2ndHandBikes": null,
        "isRental": null,
        "isBikeRepair": null,
        "sellsAccessories": true
    }, {
        "shopId": "OTky",
        "shopName": "Nuqwedsaqwas;",
        "shopEmail": null,
        "shopPhone": "+45 35 35 81 82",
        "shopAddress": "N&oslash;rrebrogade 46, 2200 K&oslash;benhavn, Denmark",
        "shopWebsite": "http:\/\/ekspresklassiskcykler.com\/",
        "pricePoint": "$$$",
        "isApproved": null,
        "latitude": "55.6895",
        "longitude": "12.5567",
        "sellsNewBikes": null,
        "sells2ndHandBikes": null,
        "isRental": null,
        "isBikeRepair": true,
        "sellsAccessories": null
    }, {
        "shopId": "OTkz",
        "shopName": "Nasdas",
        "shopEmail": null,
        "shopPhone": "+45 32 57 89 79",
        "shopAddress": "Holmbladsgade 27, 2300 K&oslash;benhavn, Denmark",
        "shopWebsite": "http:\/\/www.amarcykelservice.dk\/",
        "pricePoint": "$$",
        "isApproved": null,
        "latitude": "55.6651",
        "longitude": "12.6062",
        "sellsNewBikes": null,
        "sells2ndHandBikes": null,
        "isRental": null,
        "isBikeRepair": null,
        "sellsAccessories": null
    }, {
        "shopId": "OTk0",
        "shopName": "Islands Brygge Cykler",
        "shopEmail": null,
        "shopPhone": "+45 38 11 22 77",
        "shopAddress": "Frederiksborggade 31, 1360 K&oslash;benhavn, Denmark",
        "shopWebsite": "http:\/\/velorbis.com\/",
        "pricePoint": "$",
        "isApproved": null,
        "latitude": "55.6851",
        "longitude": "12.5674",
        "sellsNewBikes": null,
        "sells2ndHandBikes": true,
        "isRental": null,
        "isBikeRepair": null,
        "sellsAccessories": null
    }, {
        "shopId": "OTk2",
        "shopName": "N\u00f8rrebro' \u00e6&lt;h1&gt;alert();&lt;\/h1&gt;",
        "shopEmail": null,
        "shopPhone": "+45 32 11 69 12",
        "shopAddress": "Rantzausgade 2, 2200 K&oslash;benhavn, Denmark",
        "shopWebsite": null,
        "pricePoint": "$$",
        "isApproved": true,
        "latitude": "55.6854",
        "longitude": "12.5532",
        "sellsNewBikes": true,
        "sells2ndHandBikes": true,
        "isRental": false,
        "isBikeRepair": null,
        "sellsAccessories": null
    }, {
        "shopId": "OTk3",
        "shopName": "N\u00f8rrebro' \u00e6&lt;h1&gt;alert();&lt;\/h1&gt;",
        "shopEmail": null,
        "shopPhone": "+45 50 32 11 00",
        "shopAddress": "&Aring;boulevard 3, 1635 K&oslash;benhavn, Denmark",
        "shopWebsite": "http:\/\/www.copenhagenbikes.dk\/",
        "pricePoint": "$$",
        "isApproved": null,
        "latitude": "55.6823",
        "longitude": "12.557",
        "sellsNewBikes": null,
        "sells2ndHandBikes": null,
        "isRental": null,
        "isBikeRepair": null,
        "sellsAccessories": null
    }];
}
