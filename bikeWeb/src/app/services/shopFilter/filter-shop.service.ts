import {Injectable} from '@angular/core';
import {Shop} from "../../entities/shop";

@Injectable({
    providedIn: 'root'
})
export class FilterShopService {

    constructor() {
    }

    filter(shops: Shop[], searchText: string, sellsNew: boolean, sells2ndHand: boolean, sellsAccessories: boolean, isRental:
        boolean, isOpenNow: boolean, is$: boolean, is$$: boolean, is$$$: boolean) {

        return shops
            .filter(it => {
                if (searchText == undefined || searchText == '')
                    return true;
                return it.shopName.toLowerCase().includes(searchText.toLowerCase());
            }).filter(it => {
                if (!isRental)
                    return true;
                return it.isRental;
            }).filter(it => {
                if (!sellsNew)
                    return true;
                return it.sellsNewBikes;
            }).filter(it => {
                if (!sells2ndHand)
                    return true;
                return it.sells2ndHandBikes;
            }).filter(it => {
                if (!sellsAccessories)
                    return true;
                return it.sellsAccessories;
            }).filter(it => {
                return true;
                // if (!isOpenNow)
                //     return true;
                // return it.sellsAccessories;
            }).filter(it => {
                if(is$) {
                    return it.pricePoint == '$';
                }
                if(is$$) {
                    return it.pricePoint == '$$';
                }
                if(is$$$) {
                    return it.pricePoint == '$$$';
                }
                return true;
            });
    }
}
