import { Injectable } from '@angular/core';
import { ShopService } from '../api/shop/shop.service';
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import { of } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ShopsDataService {
  shopsNearby: any [];
  allShops: any [];

  constructor( private api: ShopService ) { }

  getShopsNearby(lat, lng) : any {
    if (this.shopsNearby == undefined){
      // console.log('from api');
      return this.api.getAllShopsNearby(lat, lng, 5)
      .map(shopsNearby => {
        this.shopsNearby = shopsNearby;
        return shopsNearby;
      })
    } else {
      // console.log('from data service');
      return of(this.shopsNearby);
    }
  }

  getAllShops() : any {
    if (this.allShops == undefined){
      // console.log('from api');
      return this.refreshAllShops();
    } else {
      // console.log('from data service');
      return of(this.allShops);
    }

  }

  refreshAllShops() : any {
    return this.api.getAllShopsBasicCategories()
    .map(allShops => {
      this.allShops = allShops;
      return allShops;
    })
  }
  
}
