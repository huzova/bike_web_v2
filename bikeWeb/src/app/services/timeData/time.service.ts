import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TimeService {

  timeData = [
    {value: 'closed', viewValue: 'closed'},
    {value: '0', viewValue: '0:00'},
    {value: '1', viewValue: '1:00'},
    {value: '2', viewValue: '2:00'},
    {value: '3', viewValue: '3:00'},
    {value: '4', viewValue: '4:00'},
    {value: '5', viewValue: '5:00'},
    {value: '6', viewValue: '6:00'},
    {value: '7', viewValue: '7:00'},
    {value: '8', viewValue: '8:00'},
    {value: '9', viewValue: '9:00'},
    {value: '10', viewValue: '10:00'},
    {value: '11', viewValue: '11:00'},
    {value: '12', viewValue: '12:00'},
    {value: '13', viewValue: '13:00'},
    {value: '14', viewValue: '14:00'},
    {value: '15', viewValue: '15:00'},
    {value: '16', viewValue: '16:00'},
    {value: '17', viewValue: '17:00'},
    {value: '18', viewValue: '18:00'},
    {value: '19', viewValue: '19:00'},
    {value: '20', viewValue: '20:00'},
    {value: '21', viewValue: '21:00'},
    {value: '22', viewValue: '22:00'},
    {value: '23', viewValue: '23:00'},
    {value: '24', viewValue: '24:00'}
  ];

  dayData = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];


  constructor() { }

}
