import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersDataService {

  admin = {
    "userId":1,
    "userMail": "a@a.com",
    "userName": "a",
    "userPassword": "a",
    "isBikeAdmin": false,
    "isAdmin": true
  };

  bikeAdmin = {
    "userId":2,
    "userMail": "b@b.com",
    "userName": "b",
    "userPassword": "b",
    "isBikeAdmin": true,
    "isAdmin": false
  };

  user = {
    "userId":3,
    "userMail": "u@u.com",
    "userName": "u",
    "userPassword": "u",
    "isBikeAdmin": false,
    "isAdmin": false
  };

  usersData = [this.admin, this.bikeAdmin, this.user];

  shopAdmin = [this.bikeAdmin];
  constructor() { }

}
