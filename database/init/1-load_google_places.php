<?php


$placesApi = "https://maps.googleapis.com/maps/api/place";
$apiKey = "AIzaSyC3Gz3KCswKa4_xCWhKeTqVrzQ8R1bJnus";
//https://maps.googleapis.com/maps/api/place/textsearch/json?input=Bike%20Shop%20Copenhagen&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=AIzaSyC3Gz3KCswKa4_xCWhKeTqVrzQ8R1bJnus
//1. all that contain Bike Shop Copenhagen
//load("${placesApi}/textsearch/json?input=Bike%20Shop%20Copenhagen&inputtype=textquery&language=en&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=${apiKey}", "bike-shops.json");
//2. all that have
//load("${placesApi}/textsearch/json?input=Bike%20Shop%20Copenhagen&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=${apiKey}&type=bicycle_store", "bike-types.json");
////3. all that contain Cykel københavn
//load("${placesApi}/textsearch/json?input=Cykel%20københavn&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=${apiKey}", "cykel-shops.json");
//load("${placesApi}/textsearch/json?input=cykler%20københavn&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=${apiKey}", "cykler-kbh.json");
//load("${placesApi}/textsearch/json?input=cykler&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=${apiKey}", "cykler.json");
load("${placesApi}/textsearch/json?input=bike%20repair&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=${apiKey}", "bike-repair.json");

function load($url, $fileName)
{
    $page = 0;
    $pageToken = '';
    $allResults = [];
    do {
        echo "page $page - $pageToken </br>";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($page === 0) { // first page
            $curlUrl = $url;
        } else {
            if (isset($pageToken) && $pageToken != null && $pageToken !== '') // there's next page
                $curlUrl = $url . "&pagetoken=" . $pageToken . "&rnd=$page";
            else // last page - there's no next token
                break;
        }

        curl_setopt($ch, CURLOPT_URL, $curlUrl);

        $result = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($result, true);
        $pageToken = $json['next_page_token'];
        if (!isset($pageToken) || $pageToken == null)
            echo "response json was $result for url $curlUrl</br>";

        $results = $json['results'];
        $allResults = array_merge($allResults, $results);
        $page++;
        sleep ( 3);
    } while ($page < 10);

    $myfile = fopen($fileName, "w") or die("Unable to open file!");
    fwrite($myfile, json_encode($allResults));
    fclose($myfile);
}
