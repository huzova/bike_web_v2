<?php


$allPlaces = [];
if ($handle = opendir('.')) {
    while (false !== ($file = readdir($handle))) {
        if ($file != "." && $file != ".." && strtolower(substr($file, strrpos($file, '.') + 1)) == 'json') {
            $data = file_get_contents($file);
            $json = json_decode($data);
            $allPlaces = array_merge($allPlaces, $json);
        }
    }
    closedir($handle);
}

echo "Original size " . count($allPlaces) . "</br>";

$uniqueKeys = [];
foreach ($allPlaces as $item) {
//    var_dump($allPlaces);
    echo $item->place_id . '</br>';
    $uniqueKeys[$item->place_id] = $item;
}
//
$fileName = 'shops-details.jsonp';
$apiKey = "AIzaSyC3Gz3KCswKa4_xCWhKeTqVrzQ8R1bJnus";

$detailsArray = [];
echo "Keys " . count($uniqueKeys) . '</br>';
foreach ($uniqueKeys as $key => $v) {
    echo $key . '</br>';

    $placeId = $key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $url = "https://maps.googleapis.com/maps/api/place/details/json?language=en&key=$apiKey&placeid=$placeId&fields=address_component,adr_address,alt_id,formatted_address,geometry,icon,id,name,permanently_closed,photo,place_id,plus_code,scope,type,url,utc_offset,vicinity,formatted_phone_number,international_phone_number,opening_hours,website,price_level,rating,review";
    echo  $url;
    curl_setopt($ch, CURLOPT_URL,$url );
    $result = curl_exec($ch);
    curl_close($ch);
    $json = json_decode($result);
    array_push($detailsArray, $json -> result);
//    break;
}

$myfile = fopen($fileName, "w") or die("Unable to open file!");
fwrite($myfile, json_encode($detailsArray));
fclose($myfile);

//$allResults = array_merge($allResults, $results);
//
//fclose($myfile);

