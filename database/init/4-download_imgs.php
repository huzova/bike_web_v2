<?php

require_once(dirname(__FILE__) . '/../../api/internal/repository/FileRepository.php');
require_once(dirname(__FILE__) . '/../../api/internal/model/Shop.php');

$folder = '../../api/upload/img/';
$apiKey = "AIzaSyC3Gz3KCswKa4_xCWhKeTqVrzQ8R1bJnus";
$photoNumber = 0;

$jsonFile = file_get_contents("shops-details.jsonp");
$shopsJsonEncoded = json_decode($jsonFile);

foreach ($shopsJsonEncoded as $shop) {
//    if ($photoNumber > 10)
//        break;

    foreach ($shop->photos as $photo) {

        $reference = $photo->photo_reference;
        $url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=3968&photoreference=$reference&key=$apiKey";
        echo "$photoNumber: $url </br>";
        $photoNumber++;

        $downloadPath = "$folder$reference.png";
        $file = fopen($downloadPath, "w+");
        set_time_limit(0);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $file);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        curl_close($ch);
        fclose($file);
    }
}

