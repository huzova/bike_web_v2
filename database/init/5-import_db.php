<?php

require_once(dirname(__FILE__) . '/../../api/internal/repository/ShopRepository.php');
require_once(dirname(__FILE__) . '/../../api/internal/model/Shop.php');
require_once(dirname(__FILE__) . '/../../api/internal/model/OpeningHours.php');
require_once(dirname(__FILE__) . '/../../api/internal/model/UserReview.php');

$jsonFile = file_get_contents("shops-details.jsonp");
$shopsJsonEncoded = json_decode($jsonFile, false, 512, JSON_UNESCAPED_UNICODE);
//var_dump($shopsJsonEncoded);
//die;
$shopRepository = ShopRepository::getInstance();
$shpid = 0;
foreach ($shopsJsonEncoded as $shopJson) {

    $shop = new Shop();
    $shop->shopName = $shopJson->name;
    $shop->shopAddress = $shopJson->formatted_address;
    $shop->shopPhone = $shopJson->international_phone_number;
    $shop->longitude = $shopJson->geometry->location->lng;
    $shop->latitude = $shopJson->geometry->location->lat;
    $shop->shopWebsite = $shopJson->website;
    $shop->pricePoint = '$';
//    var_dump($shop); echo '</br>'; var_dump($shopJson);
//    die;
    $shopId = $shopRepository->create($shop);
    $shop->shopId = $shopId;
    // save opening times
    $shop->openingHours = [];
    if (is_array($shopJson->opening_hours->periods)) {
        foreach ($shopJson->opening_hours->periods as $period) {
            $day = $period->open->day;
            $open = $period->open->time;
            $close = $period->close->time;

            $openingHours = new OpeningHours();
            $openingHours->shopId = $shopId;
            switch ($day) {
                case 1:
                    $openingHours->day = "Monday";
                    break;
                case 2:
                    $openingHours->day = "Tuesday";
                    break;
                case 3:
                    $openingHours->day = "Wednesday";
                    break;
                case 4:
                    $openingHours->day = "Thursday";
                    break;
                case 5:
                    $openingHours->day = "Friday";
                    break;
                case 6:
                    $openingHours->day = "Saturday";
                    break;
                case 0:
                    $openingHours->day = "Sunday";
                    break;
            }

            $openingHours->openTime = $open;
            $openingHours->closeTime = $close;

            array_push($shop->openingHours, $openingHours);
        }
        $shopRepository->saveShopOpeningHours($shop);
    }
    // save photos
    $shop->images = [];
    if (is_array($shopJson->photos)) {
        foreach ($shopJson->photos as $photo) {
            $reference = $photo->photo_reference;
            $folder = 'api/upload/img/';
            $imgPath = "$folder$reference.png";
            array_push($shop->images, $imgPath);
        }

        $shopRepository->saveShopImages($shop);
    }
    $reviews = [];
    if (is_array($shopJson->reviews)) {
        foreach ($shopJson->reviews as $reviewData) {
            $review = new UserReview();
            $review->userId = null;
            $review->shopId = $shop->shopId;
            $review->review = $reviewData->text;
            $review->rating = $reviewData->rating;

            array_push($reviews, $review);
        }

        $shopRepository->saveShopReviews($reviews);

    }

    echo $shpid++;
    echo ':</br>' . json_encode($shop);
    echo '</br></br>';

//    break;
}
