<?php
require_once(dirname(__FILE__) . '/../../api/internal/repository/ShopRepository.php');
require_once(dirname(__FILE__) . '/../../api/internal/model/Shop.php');
require_once(dirname(__FILE__) . '/../../api/internal/model/OpeningHours.php');
require_once(dirname(__FILE__) . '/../../api/internal/model/UserReview.php');

$jsonFile = file_get_contents("shops-details.jsonp");
$shopsJsonEncoded = json_decode($jsonFile, false, 512, JSON_UNESCAPED_UNICODE);
$shpid = 0;
foreach ($shopsJsonEncoded as $shopJson) {

    $shopPhone = $shopJson->international_phone_number;
    $longitude = $shopJson->geometry->location->lng;
    $latitude = $shopJson->geometry->location->lat;
    
    echo "update bikeshops set longitude=$longitude, latitude=$latitude where shop_phone='$shopPhone';</br>";

}
